/*
 * cyarn.h -- add a comment about this file
 * 
 * This file is a part of some project.
 * 
 * Copyright (C) 2018 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef CYARN_H
# define CYARN_H

typedef struct cyarn_context_ *cyarn_context;
typedef enum {
    CYARN_JVM_EXCEPTION,
    CYARN_OK
} cyarn_status;

extern int
cyarn_init (cyarn_context *penv, const char *cyarnjarloc);

extern cyarn_status
cyarn_get_error_status (cyarn_context env);

extern void
cyarn_clear_error_status (cyarn_context env);

extern int
cyarn_invoke_aTest_method (cyarn_context env, const char *message);

extern int 
cyarn_terminate (cyarn_context env);

#endif /* ! CYARN_H */
