#include <jni.h>
#include "cyarn.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct cyarn_context_ {
    cyarn_status status;
    JavaVM *jvm;
    JNIEnv *env;
};

int
cyarn_init (cyarn_context *pctx, const char *cyarnjarloc)
{
  cyarn_context ctx = malloc (sizeof (*ctx));
  *pctx = NULL;

  if (ctx != NULL)
    {
      static const char *jcpopt = "-Djava.class.path=";
      JavaVMInitArgs vm_args;
      JavaVMOption options[2];
      options[0].optionString =
          calloc (strlen (cyarnjarloc) + strlen (jcpopt) + 1, sizeof(char));
      strcat (options[0].optionString, jcpopt);
      strcat (options[0].optionString, cyarnjarloc);
      options[1].optionString = "-verbose:class";

      vm_args.version = JNI_VERSION_1_8;
      vm_args.nOptions = sizeof (options) / sizeof (options[0]);
      vm_args.options = options;
      vm_args.ignoreUnrecognized = JNI_FALSE;

      /* load and initialize a Java VM, return a JNI interface pointer in ctx */
      if (JNI_CreateJavaVM (&ctx->jvm, (void **) &ctx->env, &vm_args) == JNI_OK)
        {
          *pctx = ctx;
          ctx->status = CYARN_OK;
        }
      else
        free (ctx);
      free (options[0].optionString);
    }

  return (*pctx != NULL);
}

cyarn_status
cyarn_get_error_status (cyarn_context env)
{
  assert (env != NULL);
  return env->status;
}

void
cyarn_clear_error_status (cyarn_context env)
{
  assert (env != NULL);
  env->status = CYARN_OK;
}

static int
s_exception_occurs (cyarn_context ctx)
{
  if ((*ctx->env)->ExceptionCheck (ctx->env) == JNI_FALSE)
    return 0;

  (*ctx->env)->ExceptionDescribe (ctx->env);
  (*ctx->env)->ExceptionClear (ctx->env);
  ctx->status = CYARN_JVM_EXCEPTION;

  return 1;
}

int
cyarn_invoke_aTest_method (cyarn_context ctx, const char *message)
{
  JNIEnv *env;
  jclass cyarnClass;
  int result = 0;

  assert (ctx != NULL);
  assert (message != NULL);

  env = ctx->env;
  cyarnClass = (*env)->FindClass (env, "fr/labri/cyarn/CYarn");

  if (cyarnClass != NULL)
    {
      jmethodID mID =
          (*env)->GetStaticMethodID (env, cyarnClass, "aTest",
                                     "(Ljava/lang/String;)I");
      if (mID != NULL)
        {
          jstring str = (*env)->NewStringUTF (env, message);
          result = (*env)->CallStaticIntMethod (env, cyarnClass, mID, str);
        }
    }

  if (s_exception_occurs (ctx))
    result = 0;
  return result;
}

int
cyarn_terminate (cyarn_context ctx)
{
  JavaVM *jvm = ctx->jvm;
  return ((*jvm)->DestroyJavaVM (jvm) == JNI_OK);
}

