#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "cyarn.h"

#ifndef CYARN_JARFILE
# error "CYARN_JARFILE is not defined"
#endif /* CYARN_JARFILE */

int
main (int argc, char **argv)
{
  cyarn_context ctx;
  int result = EXIT_FAILURE;

  if (cyarn_init (&ctx, CYARN_JARFILE))
    {
      const char *msg1 = "ah ah ah !";
      const char *msg2 = "oh oh!";
      int res = cyarn_invoke_aTest_method (ctx, msg1);
      assert (res == strlen (msg1));
      res = cyarn_invoke_aTest_method (ctx, msg2);
      assert (res == strlen (msg2));

      if (cyarn_terminate (ctx))
        result = EXIT_SUCCESS;
    }

  return result;
}
