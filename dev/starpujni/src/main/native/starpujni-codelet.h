#ifndef __STARPUJNI_CODELET__
# define __STARPUJNI_CODELET__

# include "starpujni-common.h"

typedef struct starpujni_codelet *starpujni_codelet_t;

EXTERN int
starpujni_codelet_init (JNIEnv *env);

EXTERN int
starpujni_codelet_terminate (JNIEnv *env);

EXTERN starpujni_codelet_t
starpujni_codelet_create (JNIEnv *env, jobject codelet);

EXTERN starpujni_codelet_t
starpujni_codelet_create_for_1_buffer (JNIEnv *env, jobject codelet,
                                       jclass hdl);

EXTERN starpujni_codelet_t
starpujni_codelet_create_for_2_buffers (JNIEnv *env, jobject codelet,
                                        jclass hdl1, jclass hdl2);

EXTERN size_t
starpujni_codelet_get_size (starpujni_codelet_t jnicl);

EXTERN int
starpujni_codelet_get_nb_buffers (starpujni_codelet_t jnicl);

EXTERN struct starpu_codelet *
starpujni_codelet_get_codelet (starpujni_codelet_t jnicl);

EXTERN void
starpujni_codelet_set_ith_buffer_interface (JNIEnv *env, starpujni_codelet_t cl,
                                            int index,
                                            jclass handle_class);

EXTERN void
starpujni_codelet_destroy (JNIEnv *env, starpujni_codelet_t cl);

#endif /* __STARPUJNI_CODELET__ */
