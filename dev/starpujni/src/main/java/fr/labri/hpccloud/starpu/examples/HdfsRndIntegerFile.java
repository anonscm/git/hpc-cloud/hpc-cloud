package fr.labri.hpccloud.starpu.examples;

import org.apache.hadoop.fs.*;

import java.io.DataOutputStream;
import java.util.EnumSet;
import java.util.Random;

public class HdfsRndIntegerFile {
    static int SEED = 42;
    static long NB_RANDOM_NUMBERS = 1000 * 1000 * 1000;


    public static void main(String[] args) throws Exception {
        generateFile(args[0]);
    }

    static void generateFile(String filename) throws Exception {
        FileContext fc = FileContext.getFileContext();
        Path path = new Path(filename);
        FSDataOutputStream out = fc.create(path, EnumSet.of(CreateFlag.CREATE));
        generateNumbersInFile((int) System.currentTimeMillis(), NB_RANDOM_NUMBERS, out);
        out.close();
    }

    public static void generateNumbersInFile(int seed, long nbNumbers, DataOutputStream out) throws Exception {
        Random rnd = new Random(seed);
        while (nbNumbers-- > 0)
            out.writeUTF("" + rnd.nextInt() + "\n");
        out.flush();
        out.close();
    }
}
