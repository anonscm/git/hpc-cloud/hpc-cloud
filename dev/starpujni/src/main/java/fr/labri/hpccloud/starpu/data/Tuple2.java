package fr.labri.hpccloud.starpu.data;

import java.util.Objects;

public final class Tuple2<U, V> {
    private U u;
    private V v;

    public Tuple2(U u, V v) {
        this.u = u;
        this.v = v;
    }
    public U _1() {
        return u;
    }

    public V _2() {
        return v;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(getClass())) {
            Tuple2<U,V> po = (Tuple2<U,V>)obj;
            return po.u.equals(u) && po.v.equals(v);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(u, v);
    }

    @Override
    public String toString() {
        return u.toString()+" " + v.toString();
    }
}
