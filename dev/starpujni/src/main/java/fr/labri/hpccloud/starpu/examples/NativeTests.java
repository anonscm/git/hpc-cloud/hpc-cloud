package fr.labri.hpccloud.starpu.examples;

import fr.labri.hpccloud.starpu.StarPU;

public class NativeTests {
    public static void main(String[] args) throws Exception {
        StarPU.init();
        System.out.println("*** Starting native tests");
        if (StarPU.runNativeTests()) {
            System.out.println("SUCCESS");
        } else {
            System.out.println("FAIL");
        }
        System.out.println("*** Ending native tests");
        StarPU.shutdown();
    }
}
