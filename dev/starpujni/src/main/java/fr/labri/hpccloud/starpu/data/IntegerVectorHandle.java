package fr.labri.hpccloud.starpu.data;

public class IntegerVectorHandle extends ScalarVectorHandle {
    protected IntegerVectorHandle(long handle) {
        super(handle);
    }

    public static IntegerVectorHandle register(int size) {
        return new IntegerVectorHandle(vectorRegisterInt(size)).setRegistered();
    }
    public int getValueAt(int index) {
        return vectorGetIntAt(nativeHandle, index);
    }
    public void setValueAt(int index, int value) {
        vectorSetIntAt(nativeHandle, index, value);
    }
}
