package fr.labri.hpccloud.starpu.data;

import java.util.Objects;

public final class Tuple3<K, U, V> {
    private K k;
    private U u;
    private V v;

    public Tuple3(K k, U u, V v) {
        this.k = k;
        this.u = u;
        this.v = v;
    }

    public K _1() {
        return k;
    }

    public U _2() {
        return u;
    }

    public V _3() {
        return v;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(getClass())) {
            Tuple3<K,U,V> po = (Tuple3<K,U,V>)obj;
            return po.u.equals(u) && po.v.equals(v) && po.k.equals(k);
        }
        return false;
    }

    public Tuple2<K, Tuple2<U,V>> toKeyPairs() {
      return new Tuple2<>(k, new Tuple2<>(u,v));
    }

    @Override
    public int hashCode() {
        return Objects.hash(k, u, v);
    }

    @Override
    public String toString() {
        return k.toString()+" "+u.toString()+" " + v.toString();
    }
}
