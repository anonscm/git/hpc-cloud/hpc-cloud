package fr.labri.hpccloud.starpu.data;

public abstract class ScalarVectorHandle<T> extends DataHandle {
    protected ScalarVectorHandle(long handle) {
        super(handle);
    }

    public int getSize() {
        return vectorGetSize(nativeHandle);
    }

}
