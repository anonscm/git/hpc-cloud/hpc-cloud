package fr.labri.hpccloud.starpu;

import fr.labri.hpccloud.starpu.data.DataHandle;

public abstract class Codelet {
    public abstract void run(DataHandle buffers[]);

    public final int getNbBuffers() {
        return getAccessModes().length;
    }

    public abstract DataHandle.AccessMode[] getAccessModes();

    public String getName() {
        return "codelet";
    }
}
