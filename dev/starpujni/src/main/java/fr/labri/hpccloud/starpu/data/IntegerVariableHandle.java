package fr.labri.hpccloud.starpu.data;

public class IntegerVariableHandle<T> extends DataHandle {
    protected IntegerVariableHandle(long handle) {
        super(handle);
    }

    public static IntegerVariableHandle register() {
        return new IntegerVariableHandle(variableRegisterInt()).setRegistered();
    }

    public int getValue() {
        return variableGetIntValue(nativeHandle);
    }

    public void setValue(int v) {
        variableSetIntValue(nativeHandle, v);
    }
}
