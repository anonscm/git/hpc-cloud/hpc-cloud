package fr.labri.hpccloud.starpu;

public class StarPUException extends Exception {
    public StarPUException() {
        super();
    }

    public StarPUException(String message) {
        super(message);
    }

    native static void throwStarPUException(String message) throws StarPUException;
}
