package fr.labri.hpccloud.starpu.examples;

import org.apache.hadoop.fs.*;

public class HdfsReadFile {
    public static void main(String[] args) throws Exception {
        FileContext fc = FileContext.getFileContext();
        Path path = new Path(args[0]);
        FileStatus status = fc.getFileStatus(path);
        BlockLocation[] locations = fc.getFileBlockLocations(path, 0, status.getLen());

        for(int i = 0; i < locations.length; i++) {
            System.out.println(locations[i]);
        }
    }
}
