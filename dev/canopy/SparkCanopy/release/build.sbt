/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
//import sbtassembly.Plugin.AssemblyKeys._

name := "SparkCanopy"

organization := "fr.labri.speeddata"

version := "1.1.2"

scalaVersion := "2.10.4"

resolvers  +=Resolver.mavenLocal

val excludeJBossNetty = ExclusionRule(organization = "org.jboss.netty")

val excludeIONetty = ExclusionRule(organization = "io.netty")

val excludeEclipseJetty = ExclusionRule(organization = "org.eclipse.jetty")

val excludeMortbayJetty = ExclusionRule(organization = "org.mortbay.jetty")

val excludeAsm = ExclusionRule(organization = "org.ow2.asm")

val excludeOldAsm = ExclusionRule(organization = "asm")

val excludeCommonsLogging = ExclusionRule(organization = "commons-logging")

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.0" % "provided"

libraryDependencies += "org.apache.cassandra" % "cassandra-all" % "2.1.0" % "compile"  excludeAll(ExclusionRule(organization ="org.codehaus.jackson"))

libraryDependencies += "com.datastax.cassandra" % "cassandra-driver-core" % "2.0.5" % "compile"excludeAll( ExclusionRule(organization ="javax.servlet"))

libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.5.1" % "provided" excludeAll(ExclusionRule(organization ="commons-beanutils"),excludeJBossNetty,excludeIONetty,excludeEclipseJetty,excludeMortbayJetty,excludeAsm,excludeOldAsm,excludeCommonsLogging)

libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "2.5.1" % "provided" excludeAll(ExclusionRule(organization ="commons-beanutils"),excludeJBossNetty,excludeIONetty,excludeEclipseJetty,excludeMortbayJetty,excludeAsm,excludeOldAsm,excludeCommonsLogging)

libraryDependencies += "fr.labri.speeddata" % "CassandraCommon" % "1.0-SNAPSHOT" % "compile" excludeAll(excludeJBossNetty,excludeIONetty,excludeEclipseJetty,excludeMortbayJetty,excludeAsm,excludeOldAsm,excludeCommonsLogging)

libraryDependencies += "fr.labri.speeddata" % "Canopy" % "1.1.1-SNAPSHOT" % "compile" excludeAll( excludeJBossNetty,excludeIONetty,excludeEclipseJetty,excludeMortbayJetty,excludeAsm,excludeOldAsm,excludeCommonsLogging)

libraryDependencies += "org.codehaus.jackson" % "jackson-core-asl" % "1.8.9"excludeAll( excludeJBossNetty,excludeIONetty,excludeEclipseJetty,excludeMortbayJetty,excludeAsm,excludeOldAsm,excludeCommonsLogging)

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.2.1" % "test"

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"

//jarName in assembly := "SparkCanopy_1.1.2.jar"

//assemblySettings

//test in assembly := {}

//mergeStrategy in assembly := {
 //   case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  //  case _ => MergeStrategy.first
  //}

