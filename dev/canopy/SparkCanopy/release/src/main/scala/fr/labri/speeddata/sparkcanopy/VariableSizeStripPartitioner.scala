/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import fr.labri.speeddata.canopy.{HeatmapVertexWithType, Canopy}
import fr.labri.speeddata.heatmapdata.HeatmapVertex
import org.apache.spark.{Partitioner, HashPartitioner}
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.SparkContext.doubleRDDToDoubleRDDFunctions

import scala.collection.immutable
import scala.collection.immutable.TreeMap
import scala.collection.mutable.ListBuffer

/**
 * Created by flalanne on 10/04/15.
 */

class VariableSizeStripPartitioner(limits:Vector[(Double,Int)],nbPart:Int) extends  HashPartitioner(nbPart) {

  //val nbStripParPart = (limits.size+1)/nbPart

  override def getPartition(key: Any): Int = {
    key match {
      case id : Int => id
      case pair : (Any, Any) =>
        pair._1 match {
          case
            id:Int => id
          case _ => super.getPartition(key)
        }

      //case index : Int => index/nbStripParPart
      case _ => super.getPartition(key)
    }

  }
}



object VariableSizeStripPartitioner {


  def fusionParts(sizes : Array[Seq[Int]],limits:Vector[(Double,Int)],pointNumberThreshold:Int): Vector[(Double, Int)] = {


    val newLimits = new ListBuffer[(Double,Int)]()
    var part = 0
    var nbPoints = 0
    var lastLimit = Double.MinValue
    var multiBandesVisited = -1
    limits.foreach(l => {
      println(l)
      println(part)
      println(nbPoints)
      println(lastLimit)
      println(multiBandesVisited)
      println(sizes(l._2).size)

      if(sizes(l._2).size>1){
        if(lastLimit==Double.MinValue ){
          if( multiBandesVisited!=l._2) {
            nbPoints = 0
            newLimits += ((l._1, part))
            part += 1
            multiBandesVisited=l._2
          }else{
            newLimits += ((l._1, part-1))
          }

        }
        else{
          nbPoints = 0
          newLimits += ((lastLimit,part))
          part+=1
          newLimits += ((l._1,part))
          lastLimit = Double.MinValue
          part+=1
          multiBandesVisited=l._2
        }
      }
      else{
        if(sizes(l._2).head>pointNumberThreshold){
          if(lastLimit!=Double.MinValue){
            newLimits += ((lastLimit,part))
            part+=1
          }//else no previous part to insert
          nbPoints=0

          newLimits += ((l._1,part))
          lastLimit = Double.MinValue
          part+=1
        }
        else
        {
          if(nbPoints+sizes(l._2).head>pointNumberThreshold){
          //inserer prev
            newLimits += ((lastLimit,part))
            part += 1
            nbPoints = 0
          }
          else
          {
            nbPoints += sizes(l._2).head
            lastLimit = l._1
          }

        }

      }

    })
    newLimits.toVector
  }


  /**
   * get the index of the median of an array of double coordinates between two indices
   * @param coords array of double coordinates
   * @param leftIndex  the left limit of the interval in which the median is searched(included)
   * @param rightIndex  the right limit of the interval (excluded)
   */
  def getMedian(coords : Array[Double],leftIndex:Int,rightIndex:Int) : Int = {
    leftIndex + (rightIndex-leftIndex)/2
  }



//  def  getStripIndex(y:Double,limits:IndexedSeq[(Int,Double)]) = {
//    if (limits.size < 128) {
//      val l = limits.find(_._2 >= y)
//      if (l.isDefined)
//        l.get._1
//      else
//        limits.size
//    }
//    else{
//      var min = 0
//      var max = limits.size-1
//      if(y>limits.last._2 )
//        limits.size
//        else
//        if(y<=limits.head._2)
//          0
//      else{
//        while(max-min>1) {
//          val mid = min + (max - min) / 2
//          if (y>limits(mid)._2){
//            min=mid
//          }
//          else
//            max = mid
//        }
//        max
//      }
//    }
//  }
  def  getStripIndex(y:Double,limits:IndexedSeq[Double]): Int = {
    if (limits.size < 2) {
      val l = limits.indexWhere(_ >= y)
      if (l != -1)
        l
      else
        limits.size
    }
    else{
      var min = 0
      var max = limits.size-1
      if(y>limits.last )
        limits.size
        else
        if(y<=limits.head)
          0
      else{
        while(max-min>1) {
          val mid = min + (max - min) / 2
          if (y>limits(mid)){
            min=mid
          }
          else
            max = mid
        }
        max
      }
    }
  }
   def  getStripIndex(y:Double,limits:Vector[Vector[Double]]): (Int, Int) = {


     val part: Int = getStripIndex(y,limits.map(_.last))
     val index = getStripIndex(y,limits(part))
     (part,index)


  }

  def getStripIndex2(y:Double,limits:Vector[(Double,Int)]):(Int,Int) = {
    val index = {
      var min = 0

      var max = limits.size - 1
      if (y > limits.last._1)
        limits.size
      else
      if (y <= limits.head._1)
        0
      else {
        while (max - min > 1) {
          val mid = min + (max - min) / 2
          if (y > limits(mid)._1) {
            min = mid
          }
          else
            max = mid
        }
        max
      }
    }
    (limits(index)._2,index)

  }

  def getMedian(coords : RDD[Double],minInterval:Double,maxInterval:Double) : Double = {

    val filtered1 = coords.filter(y => y >= minInterval && y <= maxInterval)
    var filtered = filtered1
    val avg: (Double, Int) = filtered.map((_, 1)).reduce((a, b) => ((a._1 * a._2 + b._1 * b._2) / (a._2 + b._2), a._2 + b._2))
    val count = avg._2
    var median = avg._1
    var nbInf = filtered1.filter(_<median).count()
    var min = minInterval
    var max = maxInterval

    while(math.abs(nbInf*2-count)>(count*0.1))
    {
      if(2*nbInf<count ) {
        min = median
        filtered = filtered.filter(y => y >= min && y<=max)
        median = filtered.mean

      }
      else{
        max = median
        filtered = filtered.filter(y => y >= min && y<=max)
        median = filtered.mean
      }
      nbInf = filtered1.filter(_<=median ).count()
      /*println("getMedian median " + median)
      println("getMedian minInterval " + minInterval)
      println("getMedian maxInterval " + maxInterval)
      println("getMedian count " + count)
      println("getMedian nbInf " + nbInf)
      println("getMedian max " + max)
      println("getMedian min " + min)
*/
    }
    median

  }

  def getMedians(coords : RDD[Double],nbParts:Int) : Seq[Double] = {
    var sizes = TreeMap[Double,Long]()
    val max = (coords.max(),coords.count())
    val min = (coords.min(),0L)
    sizes += (max,min)




    val v: IndexedSeq[Double] = Range(1,nbParts).map(indexMedian => {
      var filtered = coords
      val count = indexMedian * max._2 /nbParts
      val partitioned = sizes.partition(_._2<count)
      var borneInf: (Double, Long) = partitioned._1.reduce((a,b) => if(a._2>b._2) a else b)
      var borneSup: (Double, Long) = partitioned._2.reduce((a,b) => if(a._2<b._2) a else b)
      if(borneSup._2==count)
        borneSup._1
      else {
        var nbInf = borneInf._2
        var median = borneInf._1
        while (nbInf != count) {

          if (nbInf < count) {
            borneInf = (median, nbInf)
            filtered = filtered.filter(y => y >= borneInf._1 && y < borneSup._1)
            median = filtered.mean

          }
          else {
            borneSup = (median, nbInf)
            filtered = filtered.filter(y => y >= borneInf._1 && y < borneSup._1)
            median = filtered.mean()
          }
          nbInf = coords.filter(_ < median).count()
          sizes += ((median, nbInf))
          println("getMedian median " + median)
          println("getMedian indexMedian " + indexMedian)
          println("getMedian count " + count)
          println("getMedian nbInf " + nbInf)
          println("getMedian BorneInf " + borneInf)
          println("getMedian borneSup " + borneSup)

        }
        median
      }
    })
    v
  }

  def getPartitionLimits(sample:Array[Double],nbPart:Int) = {
    val size = sample.length
    Range(1,nbPart).map(id=> {
      val sampleId =  math.round(id *size.toDouble/nbPart).toInt
      (sample(sampleId),id-1)
    })
  }


def getPartitionLimits2(sample:Array[Double],nbPart:Int): immutable.IndexedSeq[Double] = {
    val size = sample.length
    val limits1 = Range(1,nbPart).map(id=> {
      val sampleId = math.round(id *size.toDouble/nbPart).toInt
      sample(sampleId)
    })

    limits1
  }

def getPartitionLimits3(sample:Array[Double],nbPart:Int): immutable.IndexedSeq[Double] = {

  val size = sample.length


  val sizes: Seq[(Double, Int)] = sample.groupBy(a => a).mapValues(_.length).toSeq.sortBy(_._1)
  val l2 = ListBuffer[Double]()
  var last = (-180.0,0)
  sizes.foreach(s => {
      last = (s._1,s._2+last._2)
      if(last._2>((l2.size+1)*size.toDouble/nbPart))
      {
        l2 += last._1

      }

    })

//    val limits1 = Range(1,nbPart).map(id=> {
//      val sampleId = math.round(id *size.toDouble/nbPart).toInt
//      sample(sampleId)
//    })
//  limits1
  l2.toVector
  }

  def getPartitionLimits4(sample:Array[Double],nbStrips:Int,nbPart:Int): Vector[(Double, Int)] = {
    val nbStripParPart = nbStrips/nbPart
    var divisions = Vector(sample)
    while(divisions.size*2<=nbStrips) {

      divisions = divisions.flatMap(s => {
        if(s.last==s.head)
          Vector(s)
        else {
          val s1 = s.partition(_ < s(s.length / 2))
          val s2 = s.partition(_ <= s(s.length / 2))

          if (math.abs(s1._1.length - s1._2.length) < math.abs(s2._1.length - s2._2.length))
            Vector(s1._1, s1._2)
          else
            Vector(s2._1, s2._2)
        }
      })
    }


//    divisions = divisions.sortBy(-_.length)
    val part = divisions.partition(array => array.head==array.last)
    val splitable = part._2.sortBy(-_.size)
    divisions = splitable.take(nbStrips-divisions.size).flatMap(s => {

      val s1 = s.partition(_ < s(s.length / 2))
      val s2 = s.partition(_ <= s(s.length / 2))

      if (math.abs(s1._1.length - s1._2.length) < math.abs(s2._1.length - s2._2.length))
        Vector(s1._1, s1._2)
      else
        Vector(s2._1, s2._2)
    }) ++ splitable.drop(nbStrips-divisions.size) ++ part._1

    val out = (divisions.map(_.last).sorted.dropRight(1) :+ (Double.MaxValue))
      .zipWithIndex.map((a: (Double, Int)) => (a._1,a._2/nbStripParPart))
      //.grouped(nbStrips/nbPart).toVector
    //println(out.flatten.size)
    //println(out.size)
    out
  }


  //  def getPartitions(rdd:RDD[HeatmapVertex],nbStrips:Int,sampleConst:Int,nbPart:Int): (RDD[(Int, HeatmapVertex)], IndexedSeq[(Int, Double)]) = {
  def getPartitions(rdd:RDD[HeatmapVertex],nbStrips:Int,sampleConst:Int,nbPart:Int): (RDD[((Int, Int), HeatmapVertex)], Vector[(Double, Int)]) = {
    val sampleSize = math.ceil(sampleConst*nbStrips * math.log(nbStrips)).toInt
    getPartitionsWithFixedSizeSample(rdd,sampleSize,nbStrips,nbPart)
  }

//  def repartition(rdd:RDD[HeatmapVertex],limits : IndexedSeq[(Int,Double)],nbPart:Int): RDD[(Int, HeatmapVertex)] = {
//  def repartition(rdd:RDD[HeatmapVertex],limits : IndexedSeq[Double],nbPart:Int): RDD[(Int, HeatmapVertex)] = {
//
//    val repart: RDD[(Int, HeatmapVertex)] = rdd.keyBy(v => {
//      val y = v.getY
//      VariableSizeStripPartitioner.getStripIndex(y,limits)
//    }).partitionBy(new VariableSizeStripPartitioner(limits,nbPart))
//    repart
//  }

  def getPartitionsWithFixedSizeSample(rdd:RDD[HeatmapVertex], sampleSize:Int,nbStrips:Int,nbPart:Int) = {
    val t1 = System.currentTimeMillis()
    val localSample: Array[HeatmapVertex] = rdd.takeSample(true,sampleSize)
    val t2 = System.currentTimeMillis()
    val sampleCoord = localSample.map(_.getY).sorted
    val t3 = System.currentTimeMillis()
    /*var limits: Seq[Int] = initialLimits
    while(limits.size-1<nbPart){
      limits = (limits ++ limits.sliding(2,1).filter(_.size==2).map((indices: Seq[Int]) => getMedian(sampleCoord,indices.head,indices.last))).sorted
    }

    limits = limits.drop(1).dropRight(1)*/
    //val l: Seq[(Double, Int)] = limits.map(index => sampleCoord(index)).zipWithIndex
    val l: Vector[(Double, Int)] = getPartitionLimits4(sampleCoord,nbStrips,nbPart)
    //println("limits " + l.mkString("\n"))
    val t4 = System.currentTimeMillis()
     //rdd.context.parallelize(l,1).map(a => a._2.toString + ";" + a._1).saveAsTextFile("testfixed")
    /*rdd.context.parallelize(localSample.map(v => {
      val y = v.getY
      val limitRight = l.find(_._1>y)

      (
        if(limitRight.isDefined)
          limitRight.get._2
        else
          l.size
      ,
      y)
    }),1).map(a => a._1.toString + ";" + a._2).saveAsTextFile("testfixed2")*/
    val repart: RDD[((Int, Int), HeatmapVertex)] = rdd.keyBy(v => {
      val y = v.getY
      VariableSizeStripPartitioner.getStripIndex2(y,l)
    }).partitionBy(new VariableSizeStripPartitioner(l,nbPart))
    val t5 = System.currentTimeMillis()
    /*println("sampling " + (t2-t1)/1000.0)
    println("sorting " + (t3-t2)/1000.0)
    println("limits " + (t4-t3)/1000.0)
    println("repart " + (t5-t4)/1000.0)*/
//    val repart = rdd.repartition()

    //println("countByKey " + repart.countByKey().mkString("\t"))

    //repart.map(a => a._1.toString + ';'+a._2.getY).repartition(1).saveAsTextFile("file:///home/flalanne/tmp/testfixed")

    (repart,l )
  }


  def getPartitionsWithRelativeSizeSample(rdd:RDD[HeatmapVertex], sampleFrac:Double): RDD[(Int, HeatmapVertex)] = {
    val sample: RDD[HeatmapVertex] = rdd.sample(true,sampleFrac)
    val sampleCoord = sample.map(_.getY)
    val initialLimits = Seq(sampleCoord.min(),sampleCoord.max())
    var limits: Seq[Double] = initialLimits
    /*while(limits.size-1<rdd.partitions.size){
      limits = (limits ++ limits.sliding(2,1).filter(_.size==2).map((indices: Seq[Double]) => getMedian(sampleCoord,indices.head,indices.last))).sorted
    }*/
    limits = getMedians(sampleCoord,rdd.partitions.length)
    //println("size l " +l.size)
    /*val repartSample = sample.keyBy(v => {
      val y = v.getY
      val limitRight = l.find(_._1>y)
      if(limitRight.isDefined)
        limitRight.get._2
      else {

          l.size
      }
    })*/
    val repart = rdd.keyBy(v => {
      val y = v.getY
      getStripIndex(y,limits.toIndexedSeq.sorted)
    }).partitionBy(new HashPartitioner(rdd.partitions.length))

    /*repart.map(a => a._1.toString + ';'+a._2.getY).repartition(1).saveAsTextFile("testrelative")
    repartSample.map(a => a._1.toString + ';'+a._2.getY).repartition(1).saveAsTextFile("testrelativeSample")
*/
    repart
  }


  def fusion(rdd:RDD[HeatmapVertex],limits:IndexedSeq[Double],nb:Long,threshold:Long): (Vector[Double], Boolean) ={
    val sampleSize = math.ceil(32*(limits.size+1) * math.log(limits.size+1)).toInt
    val sample: Array[Double] = rdd.takeSample(true,sampleSize).map(_.getY)
    val fraction = sample.length/nb.toDouble
//    val nbBandesParPartition = (limits.size+1)/rdd.partitions.length
//    var sizes: Seq[Seq[(Double, Int)]] = sample.toSeq.groupBy(y => {
//      val index: Int = getStripIndex(y, limits)
//      index/nbBandesParPartition
//    }).toSeq.map(part => {
//      part._2.groupBy(y => {
//        val index: Int = getStripIndex(y, limits)
//        if(index<limits.size) limits(index) else Double.MaxValue
//      }).mapValues(_.size).toSeq
//    })
//    while(sizes.map((a: Seq[(Double, Int)]) =>  a.size > 1 && a.map(_._2).max < threshold * fraction).reduce(_||_) ){
//      sizes = sizes.map((part: Seq[(Double, Int)]) => {
//        if(part.size > 1 && part.map(_._2).max < threshold * fraction){
//          part.grouped(2).map(a => (a.last._1,a.map(_._2).sum)).toSeq
//
//        }
//        else
//          part
//      })
//    }
//    val limits2 = sizes.flatten.map(_._1).sorted.toVector
     var sizes: Seq[(Double, Int)] = sample.toSeq.groupBy(y => {
      val index: Int = getStripIndex(y, limits)
      if(index<limits.size) limits(index) else Double.MaxValue
      }).mapValues(_.size).toSeq

    sizes ++= (limits :+ Double.MaxValue).filter(y => sizes.find(_._1==y).isEmpty).map(y =>(y,1))
    sizes = sizes.sortBy(_._1)
//    println("sizes.size "+ sizes.size)

    while(sizes.size >= 2*rdd.partitions.length && sizes.map(_._2).max < threshold * fraction){
      sizes = sizes.grouped(2).map(a => (a.last._1,a.map(_._2).sum)).toSeq.sortBy(_._1)
//      println("sizes.size "+ sizes.size)

    }
    val limits2 = sizes.map(_._1).sorted.toVector


    (if(limits2.last<360.0)
      limits2
    else
      limits2.dropRight(1),sizes.map(_._2).max < threshold * fraction)

//      sizes = sizes.grouped(2).map(a => (a.head._1,a.map(_._2).sum)).toArray
//    }


/*
    var test1: Array[(Double, Int)] = src
      .mapPartitions( it => {
      val out = it.toSeq.groupBy(v => {
        val index = VariableSizeStripPartitioner.getStripIndex(v.getY, limits)
        val y = if(index==limits.size) 181.0 else limits(index)
        y
      }).mapValues(_.size)
      out.iterator
    }).collect().sortBy(_._1)
    while(test1.map(_._2).max<minPointsPerStrip && test1.size>1){
      test1 = test1.grouped(2).map(a => (a.head._1,a.map(_._2).sum)).toArray
    }
    val limits2 = test1.map(_._1).sorted
    */
  }

}
