/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import org.apache.spark.HashPartitioner
import fr.labri.speeddata.heatmapdata.HeatmapVertex

class StripPartitioner(nbPart:Int ) extends HashPartitioner(nbPart) {
	
	override def getPartition(key:Any) = {
		key match {
      case vertexInStrip: (Int, HeatmapVertex) =>
        super.getPartition(vertexInStrip._1)

      case _ => super.getPartition(key)
    }
	}
	

}