/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import fr.labri.speeddata.canopy.{PointType, HeatmapVertexWithType, Canopy}
import fr.labri.speeddata.heatmapdata.{PositionDataWritable, PositionData, HeatmapVertexWritableComparable, HeatmapVertex}
import org.apache.spark.serializer.KryoRegistrator
import com.esotericsoftware.kryo.Kryo
import org.apache.hadoop.yarn.conf.YarnConfiguration

/**
 * Created by flalanne on 15/05/14.
 */
class HeatMapKryoRegistrator extends KryoRegistrator {
  override def registerClasses(kryo: Kryo) {
    kryo.register(classOf[YarnConfiguration], new YarnConfSerializer)
    kryo.register(classOf[HeatmapVertex])
    kryo.register(classOf[Canopy])
    kryo.register(classOf[HeatmapVertexWithType])
    kryo.register(classOf[HeatmapVertexWritableComparable])
    kryo.register(classOf[PositionData])
    kryo.register(classOf[PositionDataWritable])
    kryo.register(classOf[PointType])
    kryo.register(classOf[CanopyFunctions])
    kryo.register(classOf[VariableSizeStripPartitioner])
//    kryo.register(classOf[Grid])
  }
}
