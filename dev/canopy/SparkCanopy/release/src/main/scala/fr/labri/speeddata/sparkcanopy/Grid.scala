/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import fr.labri.speeddata.canopy.Canopy
import fr.labri.speeddata.heatmapdata.HeatmapVertex

import scala.collection.immutable.IndexedSeq
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
 * Created by flalanne on 04/05/15.
 */

class Grid(gridSize:Double) {//min grid size 360/2^32
  val map : mutable.HashMap[Long,ListBuffer[Canopy]]= mutable.HashMap()
  val shifts : Seq[(Int,Int)]  = Seq((-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1))

  def set(value:Canopy){
    val index: Long = (math.floor((value.getCenter.getX + 180.0) / gridSize).toLong << 32) |
      math.floor((value.getCenter.getY + 180.0) / gridSize).toLong
    val bucket = map.get(index)
    if(bucket.isEmpty)
      map += ((index,  ListBuffer(value)))
    else
      bucket.get += value
  }

  private def get(x:Double,y:Double): Option[ListBuffer[Canopy]] = {
    map.get((math.floor((x + 180.0) / gridSize).toLong <<32 )| //first 32 bits : X
      math.floor((y + 180.0) / gridSize).toLong) //last 32 bits Y

  }

  def covered(x:Double,y:Double,distance:Double): Option[Canopy] = {

    val bucket = get(x, y)
    val covering = bucket.getOrElse(ListBuffer[Canopy]()).find((v: Canopy) => {
      val squareDist = (v.getCenter.getX - x) * (v.getCenter.getX - x) + (v.getCenter.getY - y) * (v.getCenter.getY - y)

      (squareDist < distance * distance)
    })
    if (covering.isEmpty) {
      //val range = Range(-1, 2)
//      val buckets = range.flatMap(i => {
//        if (x + 180.0+ i * gridSize >= 0  && i * gridSize + x+ 180.0  <= 360.0)
//          range.map(j => {
//            if (y + 180.0+ j * gridSize >= 0 && y + 180.0+ j * gridSize <= 360.0)
//              get(x + i * gridSize, y + j * gridSize)
//            else
//              None
//          })
//        else
//          None
//      }).flatten.flatten
      val buckets = shifts.map(shift => {
        val i = shift._1
        val j = shift._2
        if (x + 180.0+ i * gridSize >= 0  && i * gridSize + x+ 180.0  <= 360.0 && y + 180.0+ j * gridSize >= 0 && y + 180.0+ j * gridSize <= 360.0)
          get(x + i * gridSize, y + j * gridSize)
        else
          None
      }).filter(_.isDefined).map(_.get).flatten
      buckets.find((v: Canopy) => {
        val squareDist = (v.getCenter.getX - x) * (v.getCenter.getX - x) + (v.getCenter.getY - y) * (v.getCenter.getY - y)
        (squareDist < distance * distance)
      })
    }

    else {
      covering
    }
  }




  def getValues(): Iterable[Canopy] = {
    map.values.flatten
  }



}
