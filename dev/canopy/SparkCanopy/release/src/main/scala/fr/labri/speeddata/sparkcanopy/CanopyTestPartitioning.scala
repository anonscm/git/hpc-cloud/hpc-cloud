/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import fr.labri.speeddata.heatmapdata.{Tile, HeatmapVertex, PositionDataWritable, HeatmapVertexWritableComparable}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{Partition, SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._

import scala.collection.immutable

/**
 * Created by flalanne on 17/04/15.
 */
object CanopyTestPartitioning {
  def main(args: Array[String]) {
    if(args.length<4)
      println("arguments : inputn path nbPart nbStrips sampleConst lvl1 ... lvln")
    val inputPath = args(0)
    val nbPart = args(1).toInt
    val nbStrips = args(2).toInt

    val sampleConst = args(3).toInt
    //val outputPathPrefix = if(args.length>4) args(4) else ""
    val minPart = args(4).toInt
    val lvls = Range(args(5).toInt,args(6).toInt,-1)
    val conf = new SparkConf()
      //.setMaster("yarn-standalone")
      .setAppName("SparkCanopy")
      //.set("spark.executor.memory", "1g")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryo.registrator", "fr.labri.speeddata.sparkcanopy.HeatMapKryoRegistrator")
      .set("spark.shuffle.consolidateFiles","true")
    val sc = new SparkContext(conf)
        sc.addSparkListener(new SparkStageListener(sc.getConf))
    val results = testPartitioning(sc,inputPath,nbPart,lvls,sampleConst,nbStrips,minPart,"")
//    sc.parallelize(results.toSeq,1).saveAsTextFile("CanopyTestResult")
    sc.stop()
    println(results.mkString("\n"))
  }

  def testPartitioning(sc:SparkContext,inputPath:String,nbPart:Int,levels:IndexedSeq[Int],sampleConst:Int,nbStrips:Int,minPart:Int,outpathPrefix:String): Seq[String] ={

    val heatMapData: RDD[(HeatmapVertexWritableComparable, PositionDataWritable)] = sc.sequenceFile[HeatmapVertexWritableComparable, PositionDataWritable](inputPath,classOf[HeatmapVertexWritableComparable],classOf[PositionDataWritable],nbPart)
    val mappedCanopies = if(heatMapData.partitions.length> 2*nbPart) heatMapData.map(SparkCanopy.mapToVertex).coalesce(nbPart) else heatMapData.map(SparkCanopy.mapToVertex)//.repartition(nbPart)
    //    mappedCanopies.count()
    //    val initialParts = mappedCanopies.mapPartitions(it => Some(it.size).iterator).collect()

    var srcLevel: RDD[HeatmapVertex] =  mappedCanopies.cache()
    //    mappedCanopies.count()
    val debut = System.currentTimeMillis()
    //val repart: (RDD[((Int, Int), HeatmapVertex)], Vector[Vector[Double]]) = VariableSizeStripPartitioner.getPartitions(srcLevel,nbStrips,sampleConst,nbPart)

    //var srcLevel2: RDD[(Int,HeatmapVertex)] = repart._1//.cache()
    val limits1: Vector[Vector[Double]] = //repart._2
    Vector()
    //srcLevel.unpersist()
    //srcLevel = repart._1.map(_._2).cache()
//    srcLevel.first()
    mappedCanopies.unpersist()
    val fin = System.currentTimeMillis()
    //val initialPartsSample = repart._1.mapPartitions(it => Some(it.size).iterator).collect()
    var l2 = limits1
    var coalesce = false
    val resultsWithSample = levels.map( lvl => {
//      println(lvl)

      val t1 = System.currentTimeMillis()

//      var tmpLevel = SparkCanopy.generateLevelSinglePass(srcLevel,lvl,l2,nbPart<nbStrips)//.cache()
//
//      if(coalesce&&tmpLevel.partitions.length>sc.defaultParallelism)
//        tmpLevel = tmpLevel.coalesce(tmpLevel.partitions.length/2).cache()
//
//      else
//        tmpLevel = tmpLevel.cache()
//      val count = tmpLevel.count()

//      val newLimits = VariableSizeStripPartitioner.fusion(tmpLevel,l2,count,100000)
//      l2 = newLimits._1
//      coalesce = newLimits._2


//        println("fusion " + (l2.size+1) + " " + tmpLevel.partitions.length)

      val t2 = System.currentTimeMillis()
//      srcLevel.unpersist()
//      srcLevel = tmpLevel
      //      val partitionsSize: Array[Int] = srcLevel.mapPartitions(it => Some(it.size).iterator).collect()
//      val maxTilePointsNumber =  srcLevel.keyBy((vertex:  HeatmapVertex) =>Tile.getTile(vertex,lvl)).mapValues(_ => 1).reduceByKey(_+_).values.max()
      //      val sum = srcLevel.aggregate(0L)((w,v) => w+v.getHits,_+_)
      //      (lvl,t2-t1,count,partitionsSize,maxTilePointsNumber,sum)
      //      (lvl,t2-t1,count,Seq(1),maxTilePointsNumber,sum)
//      (lvl,t2-t1,count,Seq(1),maxTilePointsNumber,0L)
                  (lvl,t2-t1,0,Seq(1),0,0L)


    })
    srcLevel.unpersist()
    srcLevel = mappedCanopies.cache()
    val resultsBasic =  levels.map( lvl => {
      println(lvl)
      val t1 = System.currentTimeMillis()
            val tmpLevel = SparkCanopy.generateLevelSinglePass(srcLevel,lvl).cache()
            val count = tmpLevel.count()
      val t2 = System.currentTimeMillis()
            srcLevel.unpersist()
            srcLevel = tmpLevel
      //      val partitionsSize: Array[Int] = srcLevel.mapPartitions(it => Some(it.size).iterator).collect()
//                        val maxTilePointsNumber =  srcLevel.keyBy(vertex =>Tile.getTile(vertex,lvl)).mapValues(_ => 1).reduceByKey(_+_).values.max()
      //            val sum = srcLevel.aggregate(0L)((w,v) => w+v.getHits,_+_)
      //
      //      (lvl,t2-t1,count,partitionsSize,maxTilePointsNumber,sum)
      //      (lvl,0,0,Seq(1),0,0L)
      (lvl,t2-t1,0,Seq(1),0,0L)
//            (lvl,t2-t1,0,Seq(1),maxTilePointsNumber,sum)
//                  (lvl,t2-t1,0,Seq(1),maxTilePointsNumber,0L)

    })
    srcLevel.unpersist()
    //    mappedCanopies.count()

    srcLevel  = mappedCanopies.cache()
    val resultsBasicWithConflicts =  levels.map( lvl => {
      //      println(lvl)
      //      val t1 = System.currentTimeMillis()
      //      val tmpLevel = SparkCanopy.generateLevel(srcLevel,lvl).cache()
      //      val count = tmpLevel.count()
      //      val t2 = System.currentTimeMillis()
      //      srcLevel.unpersist()
      //      srcLevel = tmpLevel
      //      val partitionsSize: Array[Int] = srcLevel.mapPartitions(it => Some(it.size).iterator).collect()
      //      val maxTilePointsNumber =  srcLevel.keyBy(vertex =>Tile.getTile(vertex,lvl)).mapValues(_ => 1).reduceByKey(_+_).values.max()
      //      val sum = srcLevel.aggregate(0L)((w,v) => w+v.getHits,_+_)
      //
      //      (lvl,t2-t1,count,partitionsSize,maxTilePointsNumber,sum)
      (lvl,0,0,Seq(1),0,0L)
      //      (lvl,t2-t1,0,Seq(1),0,0L)


    })
    srcLevel.unpersist()
    //    mappedCanopies.count()
    //
    srcLevel  = mappedCanopies.cache()
    val resultsBasicWithConflictsCanopy =  levels.map( lvl => {
//      println(lvl)
      val t1 = System.currentTimeMillis()
//                  val tmpLevel = SparkCanopy.generateLevelWithCanopy(srcLevel,lvl).cache()
//                  val count = tmpLevel.count()
      val t2 = System.currentTimeMillis()
//                  srcLevel.unpersist()
//                  srcLevel = tmpLevel
      //            val partitionsSize: Array[Int] = srcLevel.mapPartitions(it => Some(it.size).iterator).collect()
//                  val maxTilePointsNumber =  srcLevel.keyBy(vertex =>Tile.getTile(vertex,lvl)).mapValues(_ => 1).reduceByKey(_+_).values.max()
      //            val sum = srcLevel.aggregate(0L)((w,v) => w+v.getHits,_+_)
      //
//                  (lvl,t2-t1,count,partitionsSize,maxTilePointsNumber,sum)
      (lvl,t2-t1,0,Seq(1),0,0L)
//                  (lvl,t2-t1,0,Seq(1),maxTilePointsNumber,0L)
      //            (lvl,t2-t1,0,Seq(1),maxTilePointsNumber,sum)

    })
    srcLevel.unpersist()
    //    mappedCanopies.count()
    val debut2 = System.currentTimeMillis()
    srcLevel = mappedCanopies.persist(StorageLevel.MEMORY_AND_DISK)
    srcLevel.partitions.foreach((part: Partition) => {
      println(part.index + " " +srcLevel.preferredLocations(part).mkString("\t"))
    })
    val repart2 =
      VariableSizeStripPartitioner.getPartitions(srcLevel,nbStrips,sampleConst,nbPart)
    //repart
    val limits: Vector[(Double, Int)] = repart2._2
    //srcLevel2 = repart2._1
    var srcLevelSeq = repart2._1.groupByKey(new VariableSizeStripPartitioner(repart2._2,nbPart)).values.map(_.toArray).persist(StorageLevel.MEMORY_AND_DISK)
    //srcLevel=repart2._1.map(_._2).cache()
    srcLevelSeq.first()
    mappedCanopies.unpersist()

    val fin2 = System.currentTimeMillis()
    var l3 = limits
    coalesce=false
    //    val initialPartsSample2 = repart._1.mapPartitions(it => Some(it.size).iterator).collect()
    val resultsBasicWithConflictsCanopySample =  levels.map( lvl => {
      println(lvl)
      //      println("input count" + srcLevel.count())
      val t1 = System.currentTimeMillis()
      //
//      var storageInfo = sc.getRDDStorageInfo.filter(_.id==srcLevelSeq.id).head
//      println("memsize " + storageInfo.memSize)
      val result = SparkCanopy.generateLevelWithCanopySamplePart2(srcLevelSeq,lvl,l3,minPart)
       var tmpLevelSeq = result._1//SparkCanopy.generateLevelWithCanopySamplePart2(srcLevelSeq,lvl,l3,nbPart)//.cache()
      l3 = result._2
       //      if(tmpLevel.partitions.length>sc.defaultParallelism)
      //        tmpLevel = tmpLevel.coalesce(tmpLevel.partitions.length/2).cache()
      //      else
      tmpLevelSeq = tmpLevelSeq.persist(StorageLevel.MEMORY_AND_DISK)
      val count = tmpLevelSeq.aggregate(0L)((w,s) => w+s.size,_+_)
//      val newLimits = VariableSizeStripPartitioner.fusion(tmpLevel,l2,count,100000)
//      l2 = newLimits._1
//      coalesce = newLimits._2

//      println("fusion " + (l2.size+1) + " " + tmpLevel.partitions.length)

      val t2 = System.currentTimeMillis()
      srcLevelSeq.unpersist()
      srcLevelSeq = tmpLevelSeq
//      storageInfo = sc.getRDDStorageInfo.filter(_.id==srcLevelSeq.id).head
//      println("memsize " + storageInfo.memSize)
      //            println(srcLevel.keyBy(v => VariableSizeStripPartitioner.getStripIndex(v.getY,l2)).mapValues(a => 1L).reduceByKey(_+_).collect().sortBy(_._1).map(_._2).mkString(";"))
      //      val partitionsSize: Array[Int] = srcLevel.mapPartitions(it => Some(it.size).iterator).collect()
//      val maxTilePointsNumber =  if(count==0) 0 else srcLevelSeq.flatMap(strip => strip.map(vertex =>(Tile.getTile(vertex,lvl),1))).reduceByKey(_+_).values.max()
//                  val sum = srcLevelSeq.aggregate(0L)((w,v: Array[HeatmapVertex]) => w+v.aggregate(0L)((w2,v2) => w2+v2.getHits,_+_),_+_)
      //      (lvl,t2-t1,count,partitionsSize,maxTilePointsNumber,sum)
      //      (lvl,0,0,Seq(1),0,0L)
//      (lvl,t2-t1,count,Seq(1),maxTilePointsNumber,0L)
//            (lvl,t2-t1,count,Seq(1),maxTilePointsNumber,sum)
            (lvl,t2-t1,count,Seq(1),0,0L)

    })
    /*(resultsBasic.map(quadruplet => "basic partitionning lvl;"+quadruplet._1+";took "+(quadruplet._2.toDouble/1000)+ "seconds and has;"+quadruplet._3 + ";positions : repartitions;" + quadruplet._4.mkString(";") ) :+  ( ";initial partitions;"+initialPartsSample.mkString(";"))) ++
    resultsWithSample.map(quadruplet => "SampleBased partitionning lvl;"+quadruplet._1+";took "+(quadruplet._2.toDouble/1000)+ "seconds and has;"+quadruplet._3 + ";positions : repartitions;" + quadruplet._4.mkString(";")) :+ (";time partitioning;"+ (fin-debut)/1000.0) :+ (";partitionsLimit;"+ repart._2.mkString(";")) :+ ( ";initial partitions;"+initialPartsSample.mkString(";"))
    */
    /*    Seq("Basic ","lvl;time;nbpos" )    ++
        resultsBasic.map(quadruplet => quadruplet._1 +";"+(quadruplet._2.toDouble/1000)+";"+quadruplet._3)    ++
        Seq("Sample "+sampleConst+";"+(fin-debut)/1000.0,"lvl;time;nbpos" )    ++
        resultsWithSample.map(quadruplet => quadruplet._1 +";"+(quadruplet._2.toDouble/1000)+";"+quadruplet._3)
    */
    Seq("nbpos", "lvl;Basic;Sample;withconflicts;withConflictsCanopy") ++
      resultsBasic.indices.map( i => {
        val basic = resultsBasic(i)
        val sample = resultsWithSample(i)
        val withconflicts = resultsBasicWithConflicts(i)
        val withconflictsCanopy = resultsBasicWithConflictsCanopy(i)
        val withconflictsCanopySample = resultsBasicWithConflictsCanopySample(i)

        basic._1 +";" +basic._3+";"+sample._3+";"+withconflicts._3+";"+withconflictsCanopy._3+";"+withconflictsCanopySample._3
      }) ++
      Seq("time", "lvl;Basic;Sample"+";withconflicts;withConflictsCanopy;withConflictsCanopySample;","-1;0;"+(fin-debut)/1000.0 + ";0;0;"+(fin2-debut2)/1000.0 ) ++
      resultsBasic.indices.map( i => {
        val basic = resultsBasic(i)
        val sample = resultsWithSample(i)
        val withconflicts = resultsBasicWithConflicts(i)
        val withconflictsCanopy = resultsBasicWithConflictsCanopy(i)
        val withconflictsCanopySample = resultsBasicWithConflictsCanopySample(i)

        basic._1 +";" +basic._2.toDouble/1000+";"+sample._2.toDouble/1000+";"+withconflicts._2.toDouble/1000+";"+withconflictsCanopy._2.toDouble/1000+";"+withconflictsCanopySample._2.toDouble/1000
      })++
      Seq("maxTileSize", "lvl;Basic;Sample;withconflicts;withConflictsCanopy;withConflictsCanopySample" ) ++
      resultsBasic.indices.map( i => {
        val basic = resultsBasic(i)
        val sample = resultsWithSample(i)
        val withconflicts = resultsBasicWithConflicts(i)
        val withconflictsCanopy = resultsBasicWithConflictsCanopy(i)
        val withconflictsCanopySample = resultsBasicWithConflictsCanopySample(i)

        basic._1 +";" +basic._5+";"+sample._5+";"+withconflicts._5+";"+withconflictsCanopy._5+";"+withconflictsCanopySample._5
      }) ++
      Seq("maxPartSize/minPartSize", "lvl;Basic;Sample;withconflicts;withConflictsCanopy;withConflictsCanopySample" ) ++
      resultsBasic.indices.map( i => {
        val basic = resultsBasic(i)
        val sample = resultsWithSample(i)
        val withconflicts = resultsBasicWithConflicts(i)
        val withconflictsCanopy = resultsBasicWithConflictsCanopy(i)
        val withconflictsCanopySample = resultsBasicWithConflictsCanopySample(i)

        basic._1 +";" +basic._4.max.toDouble/math.max(basic._4.min,1)+";"+sample._4.max.toDouble/math.max(sample._4.min,1)+";"+withconflicts._4.max.toDouble/math.max(withconflicts._4.min,1)+";"+withconflictsCanopy._4.max.toDouble/math.max(withconflictsCanopy._4.min,1)+";"+withconflictsCanopySample._4.max.toDouble/math.max(withconflictsCanopySample._4.min,1)
      })++
      Seq("sum", "lvl;Basic;Sample;withconflicts;withConflictsCanopy;withConflictsCanopySample" ) ++
      resultsBasic.indices.map( i => {
        val basic = resultsBasic(i)
        val sample = resultsWithSample(i)
        val withconflicts = resultsBasicWithConflicts(i)
        val withconflictsCanopy = resultsBasicWithConflictsCanopy(i)
        val withconflictsCanopySample = resultsBasicWithConflictsCanopySample(i)

        basic._1 +";" +basic._6+";"+sample._6+";"+withconflicts._6+";"+withconflictsCanopy._6+";"+withconflictsCanopySample._6
      })
  }

}
