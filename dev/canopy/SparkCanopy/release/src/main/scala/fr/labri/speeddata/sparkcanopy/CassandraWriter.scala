/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import org.apache.spark.rdd.RDD



import java.nio.ByteBuffer

import fr.labri.speeddata.heatmapdata.{Tile, HeatmapVertex}
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import fr.labri.speeddata.cassandra.heatmap.HeatMapCassandraClient
import org.apache.hadoop.mapred.JobConf
import org.apache.hadoop.conf.Configuration
import org.apache.cassandra.hadoop.cql3.CqlOutputFormat
import org.apache.cassandra.hadoop.cql3.CqlConfigHelper
import org.apache.cassandra.hadoop.ConfigHelper
import org.apache.cassandra.dht.Murmur3Partitioner
import org.apache.cassandra.utils.ByteBufferUtil
import scala.collection.mutable
import scala.collection.JavaConverters._
import java.util

/**
 * Created by flalanne on 15/05/14.
 */
class CassandraWriter(node:String,keyspace:String) {
  var client = new HeatMapCassandraClient(node)

  if(!client.getHeatMapKeyspaces.contains(keyspace))
    client.createBulkSchema(keyspace)
  var jobConf = new JobConf(new Configuration(), getClass)
  jobConf.setOutputFormat(classOf[CqlOutputFormat])
  jobConf.setOutputKeyClass(classOf[java.util.Map[String,ByteBuffer]])
  jobConf.setOutputValueClass(classOf[java.util.List[ByteBuffer]])
  CqlConfigHelper.setOutputCql(jobConf, "update ".+(keyspace).+(".").+("positions set hits = ?"))
  ConfigHelper.setOutputColumnFamily(jobConf, keyspace, "positions")
  ConfigHelper.setOutputPartitioner(jobConf, classOf[Murmur3Partitioner].getName)
  ConfigHelper.setOutputInitialAddress(jobConf, node)
  ConfigHelper.setWriteConsistencyLevel(jobConf, "ALL")
  ConfigHelper.setReadConsistencyLevel(jobConf, "ALL")


  def saveHeatMapRDD(level: (Int, RDD[HeatmapVertex])): Unit = {

    val out: RDD[(util.Map[String, ByteBuffer], util.List[ByteBuffer])] = level._2.map(b => {

      //val m = new java.util.HashMap[String, ByteBuffer]()
      val m = mutable.Map[String,ByteBuffer]()
      m.put("agglvl", ByteBufferUtil.bytes(level._1))
      val t = Tile.getTile(b, level._1)
      m.put("tilex", ByteBufferUtil.bytes(t.getX))
      m.put("tiley", ByteBufferUtil.bytes(t.getY))
      m.put("x", ByteBufferUtil.bytes(b.getX))
      m.put("y", ByteBufferUtil.bytes(b.getY))
      /*val arrayList = new java.util.ArrayList[ByteBuffer]()
      arrayList.add(ByteBufferUtil.bytes(b.weight.toLong))*/
      (m.asJava, /*arrayList*/List(ByteBufferUtil.bytes(b.getHits.toLong)).asJava)
    })
    out.saveAsHadoopDataset(jobConf)
  }

}
