/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import java.lang.Math._

import com.github.varunpant.quadtree.QuadTree
import fr.labri.speeddata.canopy.{Canopy, HeatmapVertexWithType, PointType}
import fr.labri.speeddata.heatmapdata.{HeatmapVertex, HeatmapVertexWritableComparable, PositionDataWritable}

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

/**
 * Created by alexandre on 26/05/14.
 */
class CanopyFunctions(dist:Double) extends Serializable {

  var distance = dist
  var strip_length = 4 * dist


  def getFirstStrip(p: HeatmapVertex): Int = {
    floor((p.getY - distance) / strip_length).toInt
  }

  def mapToStrips(p: (HeatmapVertexWritableComparable, PositionDataWritable))= {
    val point = p._1.get()
    val strip =  floor((point.getY - distance) / strip_length).toInt
     (strip, new HeatmapVertex(point.getX, point.getY, p._2.get().getNbPages))
  }

  def reduceStrip(points : Iterable[HeatmapVertex]) : Iterator[HeatmapVertexWithType] = {
    val tree = new QuadTree[Canopy](-180.0,-180.0,180.0,180.0)
    for(point <- points) {
      val intersect = tree.searchWithin(point.getX-distance, point.getY-distance, point.getX+distance, point.getY+distance).find(_.getValue.isIn(point))
      intersect match {
        case Some(p) => p.getValue.add(point)
        case None => tree.set(point.getX, point.getY, new Canopy(point, distance, distance))
      }
    }

    var list = new ListBuffer[HeatmapVertexWithType]
    for(canopy <- tree.getValues.asScala) {
      list += new HeatmapVertexWithType(canopy.getCenter, PointType.CENTER)
      val v = new HeatmapVertexWithType()

      val iterPoints = canopy.getPoints.asScala

      for(vertex <- iterPoints) {
        list += new HeatmapVertexWithType(vertex, PointType.POINT)
      }
    }

    list.iterator
  }

  /**
   * canopy clustering on a strip but outputs Canopy instead of HeatmapVertexWithType
   * @param points
   * @return
   */
  def reduceStripCanopy(points : Iterator[HeatmapVertex]) : Iterator[Canopy] = {
    val tree = new QuadTree[Canopy](-180.0,-180.0,180.0,180.0)
    points.foreach(point =>  {
      val intersect = tree.searchWithin(point.getX-distance, point.getY-distance, point.getX+distance, point.getY+distance).find(_.getValue.isIn(point))
      intersect match {
        case Some(p) => p.getValue.add(point)
        case None => tree.set(point.getX, point.getY, new Canopy(point, distance, distance))
      }
    })
    tree.getValues.asScala.toSeq
    .iterator
//    val grid = new Grid (2.0*dist)
//    points.foreach(point =>  {
//      val intersect = grid.covered(point.getX, point.getY, distance)
//      intersect match {
//        case Some(p) => p.add(point)
//        case None => grid.set(  new Canopy(point, distance, distance))
//      }
//    })
//    grid.getValues
//    .iterator
  }




  /**
   * reduce a strip and output result as vertices (to ignore conflicts and process the canopy in a single pass)
   * @param points points in a strip
   * @return centers in this strip
   */
  def reduceStripSinglePass(points:Iterable[HeatmapVertex]) : Iterator[HeatmapVertex] = {

      reduceStripSinglePass(points.iterator)
  }

  /**
   * same as reduceStripSinglePass but uses a Grid instead of a QuadTree
   * Currently looks worse than reduceStripSinglePass
   * @param points
   * @return
   */

  def reduceStripSinglePassWithGrid(points:Iterable[HeatmapVertex]) : Iterator[HeatmapVertex] = {

    val grid = new Grid(dist*2.0)
    points.foreach( point => {
      val intersect = grid.covered(point.getX , point.getY , distance)
      intersect match {
        case Some(p) => p.add(point)
        case None => grid.set(  new Canopy(point, distance, distance))
      }
    })
    val it = grid.getValues.map( (canopy: Canopy) => {
      val weightOfCenter = canopy.getPoints.asScala.aggregate(0L)((weight,vertex) => weight+vertex.getHits,_+_)
      val center = canopy.getCenter
      new HeatmapVertex(center.getX,center.getY,weightOfCenter+center.getHits)
    }).iterator
    it
  }

  /**
   * reduce a strip and output result as vertices (to ignore conflicts and process the canopy in a single pass)
   * @param points points in a strip
   * @return centers in this strip
   */
  def reduceStripSinglePass(points:Iterator[HeatmapVertex]) : Iterator[HeatmapVertex] = {

    val tree = new QuadTree[Canopy](-180.0,-180.0,180.0,180.0)

    points.foreach( point => {

      val intersect = tree.searchWithin(point.getX-distance, point.getY-distance, point.getX+distance, point.getY+distance).find(_.getValue.isIn(point))
      intersect match {
        case Some(p) => p.getValue.add(point)
        case None => tree.set(point.getX, point.getY, new Canopy(point, distance, distance))
      }
    })
    val it = tree.getValues.asScala.map( (canopy: Canopy) => {
      val weightOfCenter = canopy.getPoints.asScala.aggregate(0L)((weight,vertex) => weight+vertex.getHits,_+_)
      val center = canopy.getCenter
      new HeatmapVertex(center.getX,center.getY,weightOfCenter+center.getHits)
    }).iterator
    it


  }




  def mapSecondStrip(p:(Int,HeatmapVertexWithType)) : Iterator[(Int,HeatmapVertexWithType)] = {
    val point = p._2
    var l = new ListBuffer[(Int,HeatmapVertexWithType)]
    point.`type` match {
      case PointType.POINT => l += ((Math.floor(point.vertex.getY/strip_length).toInt, point))

      case PointType.CENTER => {
        val strip = Math.floor(point.vertex.getY/strip_length).toInt
        l += ((strip, point))
        val cdist = ((point.vertex.getY%strip_length)+strip_length)%strip_length
        //println(point.vertex + " with cdist=" + cdist + "in strip "+strip)
        if (cdist <= distance) {
          //println("and also in strip " + (strip-1))
          l += ((strip-1, point))
        }
        else if (cdist >= strip_length - dist) {
          //println("and also in strip " + (strip+1))
          l += ((strip+1, point))
        }
      }
    }

    l.iterator
  }

  /**
   * affects canopies centers to second pass used to remove conflicts between strips
   * @param p
   * @return
   */
  def mapSecondStripCanopy(p:Canopy) : Iterator[(Int,Canopy)] = {
    val strip = Math.floor(p.getCenter.getY/strip_length).toInt
    val cdist = ((p.getCenter.getY%strip_length)+strip_length)%strip_length
    if(cdist >= strip_length - dist)
      Seq((strip,p),(strip+1,new Canopy(p.getCenter,distance,distance))).iterator
    else
      Some((strip,p)).iterator
  }





  def reduceConflicts(points:(Int, Iterable[HeatmapVertexWithType])) : Iterator[(HeatmapVertex, Long)] = {
    val tree = new QuadTree[Canopy](-180.0,-180.0,180.0,180.0)
    val pointsList = new ListBuffer[HeatmapVertex]
    val centersList = new ListBuffer[HeatmapVertex]
    //println(points._2.size + " points total")
    for(p <- points._2) {
      p.`type` match {
        case PointType.POINT => pointsList += p.vertex
        case PointType.CENTER => {
          val cdist = ((p.vertex.getY%strip_length)+strip_length)%strip_length
          if (cdist < distance || cdist > 2* distance) {
            if(tree.searchWithin(p.vertex.getX-distance, p.vertex.getY-distance, p.vertex.getX+distance, p.vertex.getY+distance).filter(_.getValue.isIn(p.vertex)).nonEmpty) println("CONFLICT !!!!!!!!!!!")
            tree.set(p.vertex.getX, p.vertex.getY, new Canopy(p.vertex, distance, distance))

          }
          else
            centersList += p.vertex

        }
      }
    }

    //*
    for(center <- centersList) {
      tree.searchWithin(center.getX-distance, center.getY-distance, center.getX+distance, center.getY+distance).find(_.getValue.isIn(center)) match {
        case Some(c) => c.getValue.add(center)
        case None => tree.set(center.getX, center.getY, new Canopy(center, distance, distance))
      }
    }

    for(point <- pointsList) {
      tree.searchWithin(point.getX-distance, point.getY-distance, point.getX+distance, point.getY+distance).find(_.getValue.isIn(point)) match {
        case Some(c) => c.getValue.add(point)
        case None => tree.set(point.getX, point.getY, new Canopy(point, distance, distance))
      }
    }

    //println(tree.getValues.asScala.count(x=>true) + " canopies found")

    tree.getValues.asScala.flatMap(c => (c.getCenter,if(points._1==Math.floor(c.getCenter.getY/strip_length).toInt) c.getCenter.getHits else 0)::Nil.union(c.getPoints.asScala.map(v => (c.getCenter, v.getHits)).toSeq)).iterator
  }


  /**
   * remove conflicts between strips using canopies
   * @param canopies canopies as given by mapSecondStripCanopy
   * @return pair (center of canopy, weight)
   */
  def reduceConflictsCanopy(canopies:(Int,Iterable[Canopy])): Iterable[(HeatmapVertex, Long)] ={
    val tree = new QuadTree[Canopy](-180.0,-180.0,180.0,180.0)
    val pointsList = new ListBuffer[HeatmapVertex]
    val canopiesList = new ListBuffer[Canopy]
    //println(points._2.size + " points total")
    canopies._2.foreach((c:Canopy) => {

      val cdist = ((c.getCenter.getY%strip_length)+strip_length)%strip_length
      if (cdist < distance || cdist > 2* distance) {
        if(tree.searchWithin(c.getCenter.getX-distance, c.getCenter.getY-distance, c.getCenter.getX+distance, c.getCenter.getY+distance).filter(_.getValue.isIn(c.getCenter)).nonEmpty) println("CONFLICTC !!!!!!!!!!!")
        tree.set(c.getCenter.getX, c.getCenter.getY,c)

      }
      else
        canopiesList += c

    })



    //*
    canopiesList.foreach(c => {
      tree.searchWithin(c.getCenter.getX-distance, c.getCenter.getY-distance, c.getCenter.getX+distance, c.getCenter.getY+distance).find(_.getValue.isIn(c.getCenter)) match {
        case Some(c2) => {
          c2.getValue.add(c.getCenter)
          pointsList ++= c.getPoints.asScala
        }
        case None => tree.set(c.getCenter.getX, c.getCenter.getY, c)
      }
    })

    for(point <- pointsList) {
      tree.searchWithin(point.getX-distance, point.getY-distance, point.getX+distance, point.getY+distance).find(_.getValue.isIn(point)) match {
        case Some(c) => c.getValue.add(point)
        case None => tree.set(point.getX, point.getY, new Canopy(point, distance, distance))
      }
    }


    tree.getValues.asScala.map(c => {
      val centerWeight = if(canopies._1==Math.floor(c.getCenter.getY/strip_length).toInt) c.getCenter.getHits else 0
      (c.getCenter,centerWeight+c.getPoints.asScala.aggregate(0L)((w,v)=> w+v.getHits,_+_))
    })
  }



  def reduceConflictsCanopySampleGrid(canopies:(Int,Iterable[Canopy]),limits:IndexedSeq[Double]): Iterable[(HeatmapVertex, Long)] ={
    val grid = new Grid(2.0 * dist)
    val pointsList = new ListBuffer[HeatmapVertex]
    val canopiesList = new ListBuffer[Canopy]
    //println(points._2.size + " points total")
    canopies._2.foreach((c:Canopy) => {
      val strip = VariableSizeStripPartitioner.getStripIndex(c.getCenter.getY+distance,limits)
      val cdist = if( strip!=0 ) c.getCenter.getY-(limits(strip-1) -distance) else c.getCenter.getY-(-180.0-distance)
      if (cdist < distance || cdist > 2* distance) {
        if(grid.covered(c.getCenter.getX, c.getCenter.getY, distance).nonEmpty) println("CONFLICTC !!!!!!!!!!!")
        grid.set(c)

      }
      else
        canopiesList += c

    })

    //*
    canopiesList.foreach(c => {
      grid.covered(c.getCenter.getX, c.getCenter.getY, distance) match {
        case Some(c2) => {
          c2.add(c.getCenter)
          pointsList ++= c.getPoints.asScala
        }
        case None => grid.set( c)
      }
    })

    for(point <- pointsList) {
      grid.covered(point.getX, point.getY, distance) match {
        case Some(c) => c.add(point)
        case None => grid.set( new Canopy(point, distance, distance))
      }
    }

    //println(tree.getValues.asScala.count(x=>true) + " canopies found")

    //tree.getValues.asScala.flatMap(c => (c.getCenter,if(points._1==Math.floor(c.getCenter.getY/strip_length).toInt) c.getCenter.getHits else 0)::Nil.union(c.getPoints.asScala.map(v => (c.getCenter, v.getHits)).toSeq)).iterator
    grid.getValues.map(c => {
      val strip = VariableSizeStripPartitioner.getStripIndex(c.getCenter.getY+distance,limits)

      val centerWeight =  c.getCenter.getHits
      (c.getCenter,centerWeight+c.getPoints.asScala.aggregate(0L)((w,v)=> w+v.getHits,_+_))
    })
  }



    def reduceConflictsCanopySample(canopies:(Int,Iterable[Canopy]),limits:IndexedSeq[Double]): Iterable[(HeatmapVertex, Long)] ={
    val tree = new QuadTree[Canopy](-180.0,-180.0,180.0,180.0)
    val pointsList = new ListBuffer[HeatmapVertex]
    val canopiesList = new ListBuffer[Canopy]
    //println(points._2.size + " points total")
    canopies._2.foreach((c:Canopy) => {
      val strip = VariableSizeStripPartitioner.getStripIndex(c.getCenter.getY+distance,limits)//limits.find( _._2>=c.getCenter.getY+distance)
      val cdist = if( strip!=0 ) c.getCenter.getY-(limits(strip-1) - distance) else c.getCenter.getY-(-180.0-distance)
      if (cdist < distance || cdist > 2* distance) {
//        if(tree.searchWithin(c.getCenter.getX-distance, c.getCenter.getY-distance, c.getCenter.getX+distance, c.getCenter.getY+distance).filter(_.getValue.isIn(c.getCenter)).nonEmpty) println("CONFLICTC !!!!!!!!!!!")
        tree.set(c.getCenter.getX, c.getCenter.getY,c)

      }
      else
        canopiesList += c

    })

    //*
    canopiesList.foreach(c => {
      tree.searchWithin(c.getCenter.getX-distance, c.getCenter.getY-distance, c.getCenter.getX+distance, c.getCenter.getY+distance).find(_.getValue.isIn(c.getCenter)) match {
        case Some(c2) => {
          c2.getValue.add(c.getCenter)
          pointsList ++= c.getPoints.asScala
        }
        case None => tree.set(c.getCenter.getX, c.getCenter.getY, c)
      }
    })

    for(point <- pointsList) {
      tree.searchWithin(point.getX-distance, point.getY-distance, point.getX+distance, point.getY+distance).find(_.getValue.isIn(point)) match {
        case Some(c) => c.getValue.add(point)
        case None => tree.set(point.getX, point.getY, new Canopy(point, distance, distance))
      }
    }


    tree.getValues.asScala.map(c => {
            //val strip = limits.find( _._2>=c.getCenter.getY+distance)

      val centerWeight =   c.getCenter.getHits
      (c.getCenter,centerWeight+c.getPoints.asScala.aggregate(0L)((w,v)=> w+v.getHits,_+_))
    })
  }

  /*
   * sort of vertex by their weight
   */
  def sortVertex(points : Iterable[HeatmapVertex]):Seq[HeatmapVertex] = {
  	points.toSeq.sortBy(vertex => -vertex.getHits())
  }
  def sortVertexWithType(points : Iterable[HeatmapVertexWithType]):Seq[HeatmapVertexWithType] = {
  	points.toSeq.sorted(Ordering[(Int, Long)].on((v:HeatmapVertexWithType) => {
  		if(v.`type` ==PointType.CENTER)
  			(0,v.vertex.getHits())
		else 
			(1,v.vertex.getHits())
		}))
  }
  

}
