/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import com.esotericsoftware.kryo.{Kryo, Serializer}
import org.apache.hadoop.io.Writable
import com.esotericsoftware.kryo.io.{Output, Input}
import org.apache.hadoop.yarn.conf.YarnConfiguration
import java.io.{DataInputStream, DataOutputStream}

/**
 * Created by flalanne on 26/05/14.
 */
class YarnConfSerializer extends Serializer[YarnConfiguration] {
  override def write(kryo: Kryo, output: Output, instance: YarnConfiguration): Unit = {
    val out= new DataOutputStream(output)
    instance.write(out)
  }

  override def read(kryo: Kryo, input: Input, t: Class[YarnConfiguration]): YarnConfiguration = {
    val conf = new YarnConfiguration()
      conf.readFields(new DataInputStream(input))
    conf

  }
}
