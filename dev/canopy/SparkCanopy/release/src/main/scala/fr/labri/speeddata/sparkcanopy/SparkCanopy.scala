/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/**
 * Created by alexandre on 02/05/14.
 */
package fr.labri.speeddata.sparkcanopy

import java.io.PrintStream

import fr.labri.speeddata.canopy.{HeatmapVertexWithType, Canopy}
import fr.labri.speeddata.heatmapdata._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext._
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.{HadoopRDD, RDD}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import scala.collection.JavaConverters._
import scala.collection.{immutable, mutable}
import scala.collection.mutable.ListBuffer


object SparkCanopy {

  def mapToVertex(p:(HeatmapVertexWritableComparable,PositionDataWritable)):HeatmapVertex = {
    val point = p._1.get()
    new HeatmapVertex(point.getX, point.getY, p._2.get().getNbPages)

  }

  /**
   * compute the level lvl from src in a single pass (compute canopy on each strips but does not reduce conflicts between strips
   * @param src the source level
   * @param lvl the id of the destination level
   * @return the computed level
   */
  def generateLevelSinglePass(src:RDD[HeatmapVertex],lvl:Int ) : RDD[HeatmapVertex]= {
    val nbPoints = 1000 * Math.pow(4.0, lvl)
    //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
    val dist = math.sqrt(360.0 * 360.0 * 2.0 / (nbPoints * math.sqrt(3.0)))
    val functions = new CanopyFunctions(dist)

    src.groupBy(functions.getFirstStrip)
      .values.flatMap(functions.reduceStripSinglePass)
  }

  /**
   * compute the level lvl from src in a single pass (compute canopy on each strips but does not reduce conflicts between strips with potentially multiple strips per partition
   * @param src
   * @param lvl
   * @param limits
   * @param multiStripPart
   * @return
   */
  def generateLevelSinglePass(src:RDD[HeatmapVertex],lvl:Int,limits:IndexedSeq[Double],multiStripPart:Boolean ) : RDD[ HeatmapVertex]= {
    val nbPoints = 1000 * Math.pow(4.0, lvl)
    //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
    val dist = math.sqrt(360.0 * 360.0 * 2.0 / (nbPoints * math.sqrt(3.0)))
    val functions = new CanopyFunctions(dist)
    if(!multiStripPart)
      src.mapPartitions(iterator => functions.reduceStripSinglePass(iterator ),preservesPartitioning = true)
    else {
      val v2 = src.mapPartitions( (it: Iterator[HeatmapVertex]) => {
        it.toSeq.groupBy(v => {
          VariableSizeStripPartitioner.getStripIndex(v.getY,limits)
        }).iterator

      })

      val v3 = v2.flatMapValues(vertices => functions.reduceStripSinglePass(vertices))
      v3.values
    }
  }


  /**
   * generate a heatmap level
   * @param src
   * @param lvl
   * @return
   */
  def generateLevel(src : RDD[HeatmapVertex], lvl : Int) : RDD[HeatmapVertex] = {
    val nbPoints = 1000 * Math.pow(4.0, lvl)
    //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
    val dist = math.sqrt(360.0*360.0*2.0/(nbPoints*math.sqrt(3.0)))
    val functions = new CanopyFunctions(dist)


    src.groupBy(functions.getFirstStrip)

      //    	.mapValues(functions.sortVertex)
      .flatMapValues(functions.reduceStrip)
      .flatMap(functions.mapSecondStrip)
      //.partitionBy(new HashPartitioner(src.partitions.size))
      .groupByKey()
      //    	.mapValues(functions.sortVertexWithType)
      .flatMap(functions.reduceConflicts)
      //partition to reduce net com
      //    	.partitionBy(new StripPartitioner(src.partitions.size,functions))
      .reduceByKey(_+_)
      .map(p=>new HeatmapVertex(p._1.getX,p._1.getY,p._2))
  }

  /**
   * generate a level (passing Canopy objects instead of HeatmapVertexWithType as intermediate data between the two passes)
   * @param src
   * @param lvl
   * @return
   */
  def generateLevelWithCanopy(src : RDD[HeatmapVertex], lvl : Int) = {
    val nbPoints = 1000 * Math.pow(4.0, lvl)
    //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
    val dist = math.sqrt(360.0 * 360.0 * 2.0 / (nbPoints * math.sqrt(3.0)))
    val functions = new CanopyFunctions(dist)


    src.groupBy(functions.getFirstStrip)

      //    	.mapValues(functions.sortVertex)
      .flatMapValues(in => functions.reduceStripCanopy(in.iterator))
      .values
      .flatMap(functions.mapSecondStripCanopy)
      //.partitionBy(new HashPartitioner(src.partitions.size))
      .groupByKey()
      //    	.mapValues(functions.sortVertexWithType)
      .flatMap(functions.reduceConflictsCanopy)
      //partition to reduce net com
      //    	.partitionBy(new StripPartitioner(src.partitions.size,functions))
      .reduceByKey(_ + _)
      .map(p => new HeatmapVertex(p._1.getX, p._1.getY, p._2))
  }

  /**
   * generate a level with pre-partitioned data (does not remove conflicts between partitions)
   * @param src
   * @param lvl
   * @param limits
   * @param nbPart
   * @return
   */
//  def generateLevelWithCanopySamplePart(src : RDD[ HeatmapVertex], lvl : Int,limits:IndexedSeq[Double],nbPart:Int): RDD[HeatmapVertex] = {
//    val nbPoints = 1000 * Math.pow(4.0, lvl)
//    //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
//    val dist = math.sqrt(360.0 * 360.0 * 2.0 / (nbPoints * math.sqrt(3.0)))
//    val functions = new CanopyFunctions(dist)
//    val partitioner = new VariableSizeStripPartitioner(limits,nbPart)
//    val minPointsPerStrip = 100000
//    if(nbPart==limits.size+1) {
//
//      src
//        .mapPartitions(in => {
//         functions.reduceStripCanopy(in)
//        .map(canopy => {
//           new HeatmapVertex(canopy.getCenter.getX,canopy.getCenter.getY,canopy.getCenter.getHits + canopy.getPoints.asScala.aggregate(0L)((w,v) => w+v.getHits,_+_))
//         })
//
//       })
//
//    }
//    else{
//
////      var test1: Array[(Double, Int)] = src
////        .mapPartitions( it => {
////        val out = it.toSeq.groupBy(v => {
////          val index = VariableSizeStripPartitioner.getStripIndex(v.getY, limits)
////          val y = if(index==limits.size) 181.0 else limits(index)
////          y
////        }).mapValues(_.size)
////          out.iterator
////      }).collect().sortBy(_._1)
////      while(test1.map(_._2).max<minPointsPerStrip && test1.size>1){
////        test1 = test1.grouped(2).map(a => (a.head._1,a.map(_._2).sum)).toArray
////      }
////      val limits2 = test1.map(_._1).sorted
//      val limits2 = limits
////      println("limits : "+ lvl + " " + limits2.size)
//
//      val v2: RDD[Seq[HeatmapVertex]] = src
//        .mapPartitions( it => {
//        val out = it.toSeq.groupBy(v => {
//          VariableSizeStripPartitioner.getStripIndex(v.getY,limits2)
//        })
//
//          out.iterator
//
//
//      })
//        .values
//
//      val v1 = v2
//        .flatMap(in => functions.reduceStripCanopy(in.iterator))
//
//      val v3: RDD[(Int, Seq[Canopy])] = v1
//        .mapPartitions(it => {
//        it.toSeq.groupBy(v => {
//          VariableSizeStripPartitioner.getStripIndex(v.getCenter.getY+ dist,limits2)
//        }).iterator
//      })
//      val v = v3
//        .mapPartitions((it: Iterator[(Int, Seq[Canopy])]) => {
//
//
//        it.flatMap((shiftedStrip: (Int, Seq[Canopy])) => {
//          val min: Double = if (shiftedStrip._1 == limits2.size) 180.0 else limits2(shiftedStrip._1)
//          val max: Double = if (shiftedStrip._1 + 1 >= limits2.size) 180.0 else limits2(shiftedStrip._1 + 1)
//          if (max - min < 4 * dist) {
//            val withoutConflictsRemoval = shiftedStrip._2.map(c => {
//              //                          val centerStrip = VariableSizeStripPartitioner.getStripIndex(c.getCenter.getY + dist,limits)
//              val centerWeight = c.getCenter.getHits
//              (c.getCenter, centerWeight + c.getPoints.asScala.aggregate(0L)((w, v) => w + v.getHits, (_ + _)))
//
//            })
//            withoutConflictsRemoval
//          }
//          else {
//            val withConflictsRemoval = functions.reduceConflictsCanopySample(shiftedStrip, limits2)
//            withConflictsRemoval
//
//          }
//        }).map(v => new HeatmapVertex(v._1.getX,v._1.getY,v._2))
//
//      })
//      v
//    }
//
//  }
  def generateLevelWithCanopySamplePart2(src : RDD[ Array[HeatmapVertex]], lvl : Int,limits:Vector[(Double,Int)],nbPart:Int): (RDD[Array[HeatmapVertex]],Vector[(Double,Int)]) = {
    val nbPoints = 1000 * Math.pow(4.0, lvl)
    //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
    val dist = math.sqrt(360.0 * 360.0 * 2.0 / (nbPoints * math.sqrt(3.0)))
    val functions = new CanopyFunctions(dist)
    //val partitioner = new VariableSizeStripPartitioner(limits,nbPart)
    val maxPointsPerStrip = 600000
    val minPointsPerStrip = 600000
    if(src.partitions.length==limits.size) {
      println("THEN")
      val max = src.map(_.size).max()
      if(max<minPointsPerStrip&&src.partitions.length>nbPart)
      {
        val limits3 = limits.map(a => (a._1,a._2/2))
        val partitioner2 = new VariableSizeStripPartitioner(limits3,limits3.last._2+1)
        generateLevelWithCanopySamplePart2(src.flatMap(_.map(v => (VariableSizeStripPartitioner.getStripIndex2(v.getY,limits3),v))).groupByKey(partitioner2).values.map(_.toArray),lvl,limits3,limits3.last._2+1)

      }
      else {
        (src.map(strip => {
          val iterable = functions.reduceStripCanopy(strip.iterator).map(canopy => {
            new HeatmapVertex(canopy.getCenter.getX, canopy.getCenter.getY, canopy.getCenter.getHits + canopy.getPoints.asScala.aggregate(0L)((w, v) => w + v.getHits, _ + _))
          }).toArray
          iterable

        }), limits)
      }
//      src
//        .mapPartitions(in => {
//         functions.reduceStripCanopy(in)
//        .map(canopy => {
//           new HeatmapVertex(canopy.getCenter.getX,canopy.getCenter.getY,canopy.getCenter.getHits + canopy.getPoints.asScala.aggregate(0L)((w,v) => w+v.getHits,_+_))
//         })

//       })

    }
    else{
      println("ELSE")
       val limits2 = limits


      var test2: RDD[Array[HeatmapVertex]] = src.mapPartitions(it => it.filter(_.nonEmpty).toSeq.sortBy(_.head.getY).grouped(2).flatMap(group => {
        if(group.map(_.size).sum>maxPointsPerStrip)
          group
        else
        {
          if(group.size==2)
            Some(group.head ++ group.last)
          else
            group
        }
      }))
//      println("nb Bandes " +test2.count())
//      println("nb part " +test2.partitions.length)
//      val stats = test2.map(_.size.toDouble).stats()
//      println("MAX " +stats.max)
//      println("MIN " +stats.min)
//      println("MEAN " +stats.mean)
//      test2.count()
      var limits3: Vector[(Double,Int)] = test2.mapPartitionsWithIndex((id: Int,it: Iterator[Array[HeatmapVertex]]) => {
        it.map(strip => (strip.map(_.getY).max,id))
      }).collect().toVector.sortBy(_._1)//.map(_._2)
      limits3 = limits3.updated(limits3.size-1,(Double.MaxValue,test2.partitions.length-1))
/*      var sizePart: Seq[(Int, Int)] = limits3.groupBy(_._2).mapValues(_.size).toSeq.sortBy(_._2)
      if(sizePart.last._2==1)
        limits3 = limits3.map(a => (a._1,a._2/2))

      //println("limits3 "+sizePart.mkString("\t"))
      val partitioner2 = new VariableSizeStripPartitioner(limits3,limits3.size)
      if(limits3.size<test2.partitions.length)
      {
        test2 = test2.flatMap(_.map(v => (VariableSizeStripPartitioner.getStripIndex2(v.getY,limits3),v))).groupByKey(partitioner2).values.map(_.toArray)
        println("test2.partitions.length " +test2.partitions.length)
      }
*/

        val test1 = test2
      .map(strip => functions.reduceStripCanopy(strip.iterator))
          .map(it => {
          it.map(canopy => {
            new HeatmapVertex(canopy.getCenter.getX, canopy.getCenter.getY, canopy.getCenter.getHits + canopy.getPoints.asScala.aggregate(0L)((w, v) => w + v.getHits, _ + _))
          }).toArray
        })

          /*
      .mapPartitionsWithIndex((id,it ) => {
          it.flatten.toArray.groupBy(v => {
            val shifted = VariableSizeStripPartitioner.getStripIndex2(v.getCenter.getY + dist,limits3)
            if(shifted._1!=id)
              (id,-1)
            else
              shifted
            }).iterator
        })
//      .flatMap((it: Iterator[Canopy]) => {
//        val out = it.toIterable.groupBy(v => {
//          val part = VariableSizeStripPartitioner.getStripIndex(v.getCenter.getY,limits3)
//          val shifted = VariableSizeStripPartitioner.getStripIndex(v.getCenter.getY+ dist,limits3)
//          if(part._1==shifted._1)
//            shifted
//          else
//            (part._1,-1)
//        })
//          out//.values
//      })//.groupByKey(partitioner2)
//      test1.count()
          val test4: RDD[HeatmapVertex] = test1
      .flatMap((shiftedStrip: ((Int, Int), Array[Canopy])) => {
          if(shiftedStrip._1._2 == -1){
            val withoutConflictsRemoval = shiftedStrip._2.map(c => {
              //                          val centerStrip = VariableSizeStripPartitioner.getStripIndex(c.getCenter.getY + dist,limits)
              val centerWeight = c.getCenter.getHits
              (c.getCenter, centerWeight + c.getPoints.asScala.aggregate(0L)((w, v) => w + v.getHits, (_ + _)))

            })
            withoutConflictsRemoval.map((v: (HeatmapVertex, Long)) => {
              new HeatmapVertex(v._1.getX,v._1.getY,v._2)
            })
          }
          else {
            val min: Double = if (shiftedStrip._1._2 == 0) -180.0 else limits3(shiftedStrip._1._2-1)._1
            val max: Double = limits3(shiftedStrip._1._2)._1
            if (max - min < 4 * dist) {
              val withoutConflictsRemoval = shiftedStrip._2.map(c => {
                //                          val centerStrip = VariableSizeStripPartitioner.getStripIndex(c.getCenter.getY + dist,limits)
                val centerWeight = c.getCenter.getHits
                (c.getCenter, centerWeight + c.getPoints.asScala.aggregate(0L)((w, v) => w + v.getHits, (_ + _)))

              })
              withoutConflictsRemoval.map(v => new HeatmapVertex(v._1.getX, v._1.getY, v._2))
            }
            else {
              val withConflictsRemoval = functions.reduceConflictsCanopySample((shiftedStrip._1._2, shiftedStrip._2), limits3.map(_._1))
              withConflictsRemoval.map(v => new HeatmapVertex(v._1.getX, v._1.getY, v._2))

            }
          }
        })
//        test4.count()
            val test: RDD[Array[HeatmapVertex]] = test4
          .mapPartitions(it => it.toArray.groupBy(v => {
              VariableSizeStripPartitioner.getStripIndex2(v.getY,limits3 )._2
        }).values.toArray.iterator)


      (test,limits3)
*/
      (test1,limits3)
    }
  }

  /**
   * generate a level with local data
   * @param src
   * @param lvl
   * @return
   */
  def generateLevelLocal(src :Iterable[HeatmapVertex], lvl: Int):Iterable[HeatmapVertex] = {
    val nbPoints = 1000 * Math.pow(4.0, lvl)
    //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
    val dist = math.sqrt(360.0*360.0*2.0/(nbPoints*math.sqrt(3.0)))
    val functions = new CanopyFunctions(dist)
    val result = src.toParArray.groupBy(functions.getFirstStrip)

      .mapValues(a => functions.reduceStrip(a.seq))
      .toSeq
      .flatMap(a => {
      a._2.map( (a._1,_))
    })
      .flatMap(functions.mapSecondStrip)
      .groupBy(_._1)
      .mapValues(_.map(_._2))
      .toSeq
      .flatMap(a => functions.reduceConflicts((a._1,a._2.seq)))
      .groupBy(_._1)
      .mapValues(_.map(_._2).sum)
      .map(p=>new HeatmapVertex(p._1.getX,p._1.getY,p._2))
    println("LVL "+lvl+" DONE")
    result.seq
  }

  def writeTiles(src : RDD[HeatmapVertex], dest : String, lvl : Int, hdfsConf : Broadcast[Configuration]) = {
    println("writeTiles")
    src.groupBy(v => Tile.getTile(v,lvl)).foreachPartition((iter: Iterator[(Tile, Iterable[HeatmapVertex])]) => {
      var hdfs = FileSystem.get(hdfsConf.value)
      iter.foreach {
        case (t, vertices) => {
          var out: PrintStream = null
          try {
            out = new PrintStream(hdfs.create(new Path(dest + "/tile" + t.getX + "-" + t.getY)))
            for (vertex <- vertices) {
              out.println(vertex.toString)
            }
          }
          finally {
            if (out != null)
              out.close()
          }
        }
      }})
  }

  def writeLevel(src : RDD[HeatmapVertex], dest : String,count: Long) = {
    println(count)
    val nbPart= {
      if(count<200000)
        1
      else
        (count/200000).toInt
    }
    src.map(v => {

      val outK = new HeatmapVertexWritableComparable()
      outK.set(v)
      val outV = new PositionDataWritable()
      outV.set(new PositionData(v.getHits))
      (outK,outV)
    })
      .coalesce(nbPart,false)
      .saveAsSequenceFile(dest)

  }

  def main(args: Array[String]) {
    val heatmapFolder = args(0)
    val dest = args(1)
    val levelStart = args(2).toInt

    val levelEnd = if(args.size > 3) args(3).toInt else levelStart

    val shouldWriteCassandra = if(args.size > 4) args(4).toBoolean else false
    val shouldSaveLevel = if(args.size > 5) args(5).toBoolean else false
    val shouldLogStats = if(args.size > 6) args(6).toBoolean else false
    val shouldWriteHDFSTiles = if(args.size > 7) args(7).toBoolean else false

    val nbPart = if(args.size > 8) args(8).toInt else 0
    val posPerPartMin = if(args.size > 9) args(9).toInt else 0
    val posPerPartMax = if(args.size > 10) args(10).toInt else 0
    val local = if(args.size > 11) args(11).toBoolean else false


    val conf = new SparkConf()
      //.setMaster("yarn-standalone")
      .setAppName("SparkCanopy")
      //.set("spark.executor.memory", "1g")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryo.registrator", "fr.labri.speeddata.sparkcanopy.HeatMapKryoRegistrator")
      .set("spark.shuffle.consolidateFiles","true")
    val sc = new SparkContext(conf)

    var broadFSConf: Broadcast[Configuration] = null
    var cassandraWriter : CassandraWriter = null

    if(shouldWriteCassandra) {
      cassandraWriter = new CassandraWriter("192.168.13.116", dest)
    }
    if(shouldWriteHDFSTiles) {
      broadFSConf = sc.broadcast(sc.hadoopConfiguration)
    }
    println(shouldWriteHDFSTiles)
    var heatmapData = if(nbPart==0) sc.sequenceFile[HeatmapVertexWritableComparable, PositionDataWritable](heatmapFolder) else sc.sequenceFile[HeatmapVertexWritableComparable, PositionDataWritable](heatmapFolder,nbPart)
    if(nbPart!=0&&heatmapData.partitions.length!=nbPart)
      heatmapData = heatmapData.repartition(nbPart)

    val mappedCanopies = heatmapData.map(mapToVertex)

    if(!local){
      var lvltruc : RDD[HeatmapVertex] = mappedCanopies
      for (level <- Range(levelStart,levelEnd-1, -1)) {
        //lvltruc.unpersist(false)


        val nbPoints = 1000 * Math.pow(4.0, level)
        val dist = math.sqrt(360.0 * 360.0 * 2.0 / (nbPoints * math.sqrt(3.0)))
        val functions = new CanopyFunctions(dist)
        if (shouldLogStats) {
          println("distance is " + dist)
          println("dist = " + functions.distance + " & strip length = " + functions.strip_length)
        }
        val temp = lvltruc

        val t = System.currentTimeMillis()
        var count=0L
        if (posPerPartMin == 0) {

          lvltruc = generateLevel(lvltruc, level).persist(StorageLevel.MEMORY_AND_DISK)
          count = lvltruc.count()
          //lvltruc = lvltruc.coalesce((count/50000).toInt).persist(StorageLevel.MEMORY_AND_DISK)
        }else
        {
          lvltruc = generateLevel(lvltruc, level)
          count = lvltruc.count()
          val partMin = math.ceil(count.toDouble / posPerPartMin).toInt
          val partMax = math.ceil(count.toDouble / posPerPartMax).toInt

          if(partMin<=lvltruc.partitions.length/2)
            lvltruc = lvltruc.coalesce(partMin).persist(StorageLevel.MEMORY_AND_DISK_SER)
          else
          if(partMax>=lvltruc.partitions.length*2)
            lvltruc = lvltruc.repartition(partMax).persist(StorageLevel.MEMORY_AND_DISK_SER)
          else
            lvltruc = lvltruc.persist(StorageLevel.MEMORY_AND_DISK_SER)
        }

        if(shouldLogStats) {
          val duration = System.currentTimeMillis()-t
          println("duration\t"+duration)
          println("nbnodes on level " + level + " : " + count)
          //println("total hits : " + lvltruc.map(_.getHits.toLong).reduce(_ + _))

        }
        if(shouldWriteCassandra)
          cassandraWriter.saveHeatMapRDD((level,lvltruc))
        if(shouldWriteHDFSTiles)
          writeTiles(lvltruc, dest+"/tiles/"+level, level, broadFSConf)
        if(shouldSaveLevel)
          writeLevel(lvltruc, dest+"/pos/"+level,count)
        temp.unpersist()
      }
    }
    else
    {

      var lvltruc = mappedCanopies.collect()
      for (level <- Range(levelStart,levelEnd-1, -1)) {
        val t = System.currentTimeMillis()

        lvltruc = generateLevelLocal(lvltruc, level).toArray
        if(shouldLogStats) {
          val duration = System.currentTimeMillis()-t
          println("duration\t"+duration)
          //println("nbnodes on level " + level + " : " + count)
          //println("total hits : " + lvltruc.map(_.getHits.toLong).reduce(_ + _))

        }
      }

    }
    sc.stop()
  }
}
