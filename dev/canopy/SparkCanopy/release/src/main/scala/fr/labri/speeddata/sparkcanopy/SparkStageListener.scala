/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import org.apache.spark.SparkConf
import org.apache.spark.executor.{DataReadMethod, TaskMetrics}
import org.apache.spark.scheduler._
import org.apache.spark.ui.jobs.JobProgressListener

import scala.collection.mutable

/**
 * Created by flalanne on 23/04/15.
 */
class SparkStageListener(conf:SparkConf) extends JobProgressListener(conf){

  override def onStageCompleted(stageCompleted: SparkListenerStageCompleted): Unit = {
    super.onStageCompleted(stageCompleted)
    //if(stageCompleted.stageInfo.name.contains("count")) {
    val taskMetrics: Iterable[TaskMetrics] = stageIdToData.get((stageCompleted.stageInfo.stageId, stageCompleted.stageInfo.attemptId)).get.taskData.values.map(_.taskMetrics).filter(_.isDefined).map(_.get)
    val totalExecTime = taskMetrics.map(_.executorRunTime).sum.toDouble
    val gcTotalTime: Double = taskMetrics.map(_.jvmGCTime).sum.toDouble
    val ratio = gcTotalTime/totalExecTime
    val name: String = stageCompleted.stageInfo.name
    if(ratio> 0.2){
      val string: String = "gctime stage " +
        name + " " +
        gcTotalTime / 1000 +
      " totalExecTime " + totalExecTime/1000 +
      " ratio " + ratio

      println(string)
    }

    val shuffle = taskMetrics.map(_.shuffleReadMetrics).filter(_.isDefined).map(_.get)
    val ratioShuffle = shuffle.map(_.localBlocksFetched).sum.toDouble/shuffle.map(_.remoteBlocksFetched).sum.toDouble
    val remote = shuffle.map(_.remoteBytesRead).sum+ taskMetrics.map(_.inputMetrics).filter(a => a.isDefined &&a.get.readMethod==DataReadMethod.Network).map(_.get.bytesRead).sum
    //if(ratioShuffle<1.0)
    if(remote != 0L)
      println("shuffle remote + networkInput " +
      name + " " + remote.toDouble/(1024*1024) + "MB" )

    println( name +" took "+ (stageCompleted.stageInfo.completionTime.get - stageCompleted.stageInfo.submissionTime.get).toDouble/1000)
    println( "min "  + taskMetrics.map(_.executorRunTime).min + " max " + taskMetrics.map(_.executorRunTime).max )
  }


}
