/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
package fr.labri.speeddata.sparkcanopy

import fr.labri.speeddata.heatmapdata.HeatmapVertex
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.scalatest._

import scala.util.Random

/**
 * Created by flalanne on 10/04/15.
 */
class Repartitioner$Test extends FlatSpec {
/*
  "getMedian" must "return an index between right (inclusive) and left (exclusive) " in {
    val a = Range(0,10).map(_.toDouble).toArray
    var m = VariableSizeStripPartitioner.getMedian(a,0,10)
    assert(m>=0)
    assert(m<10)
    m = VariableSizeStripPartitioner.getMedian(a,0,5)
    assert(m>=0)
    assert(m<5)
    m = VariableSizeStripPartitioner.getMedian(a,5,10)
    assert(m>=5)
    assert(m<10)
    m = VariableSizeStripPartitioner.getMedian(a,5,6)
    assert(m>=5)
    assert(m<6)
  }

  "getMedian" must "return the median " in {
    val a = Range(0,10).map(_.toDouble).toArray
    var m = VariableSizeStripPartitioner.getMedian(a,0,10)
    assert(a.indices.filter(_<=m).map(a(_)).max<=a(m))
    assert(a.indices.filter(_>=m).map(a(_)).min>=a(m))
    m = VariableSizeStripPartitioner.getMedian(a,0,5)
    assert(a.indices.filter(_<=m).map(a(_)).max<=a(m))
    assert(a.indices.filter(_>=m).map(a(_)).min>=a(m))
    m = VariableSizeStripPartitioner.getMedian(a,5,10)
    assert(a.indices.filter(_<=m).map(a(_)).max<=a(m))
    assert(a.indices.filter(_>=m).map(a(_)).min>=a(m))
    m = VariableSizeStripPartitioner.getMedian(a,5,6)
    assert(a.indices.filter(_<=m).map(a(_)).max<=a(m))
    assert(a.indices.filter(_>=m).map(a(_)).min>=a(m))
  }

  "getPartitionsWithFixedSample" must "return nearly equally distributed RDD" in {
    System.clearProperty("spark.master.port")
    val rand = new Random
    val conf = new SparkConf().setMaster("local[8]").setAppName("test").set("spark.driver.host","147.210.129.73").set("spark.ui.port","15685")
    val sc = new SparkContext(conf)
    Seq("spark", "org.eclipse.jetty", "akka").map(Logger.getLogger).foreach(_.setLevel(Level.OFF))
    Logger.getRootLogger.setLevel(Level.OFF)
    val r = Range(0,1000).map(i => {
      new HeatmapVertex(rand.nextDouble(),rand.nextGaussian() ,rand.nextLong())
    })
    val rdd = sc.parallelize(r,4)
    val reparts = Range(0,20).map(lvl => {
      val nbPoints = 1000 * Math.pow(4.0, lvl)
      //val dist = 36./(Math.sqrt(10) * Math.pow(2, lvl))
      val dist = math.sqrt(360.0 * 360.0 * 2.0 / (nbPoints * math.sqrt(3.0)))
      val func = new CanopyFunctions(dist)
      val sizes1 = rdd.keyBy(func.getFirstStrip).groupByKey().mapPartitions(it => Some(it.map(_._2.size).sum).iterator).collect()

      println("sizes1 " + sizes1.mkString("\t"))

      val min1 = sizes1.min
      val max1 = sizes1.max
      val repart1 = max1.toDouble / min1
      repart1
    })
    val repartitioned: RDD[(Int, Iterable[HeatmapVertex])] = VariableSizeStripPartitioner.getPartitionsWithFixedSizeSample(rdd,100,4)._1.groupByKey()
    val sizes: RDD[Int] = repartitioned.mapPartitions(it => Some(it.map(_._2.size).sum).iterator)
    println("sizes " + sizes.collect().mkString("\t"))
    val min = sizes.min
    val max = sizes.max
    sc.stop()
    println(max.toDouble/min)
    assert(min*3>max)
    val d = if(min==0)max.toDouble else max.toDouble / min
    /*reparts.foreach(repart1 => {
      assert(repart1 > d)
    })*/
  }

    "getPartitionsWithRelativeSample" must "return nearly equally distributed RDD" in {
      System.clearProperty("spark.master.port")

      val rand = new Random
      val conf = new SparkConf().setMaster("local[8]").setAppName("test").set("spark.ui.port","15685").set("spark.driver.host","147.210.129.73")
      val sc = new SparkContext(conf)
      Seq("spark", "org.eclipse.jetty", "akka").map(Logger.getLogger).foreach(_.setLevel(Level.OFF))
      Logger.getRootLogger.setLevel(Level.OFF)

      val r = Range(0,1000).map(i => {
        new HeatmapVertex(rand.nextDouble(),rand.nextGaussian(),rand.nextLong())
      })
      val rdd = sc.parallelize(r,4)
      val sizes1 = rdd.keyBy(_.getY/3.6).groupByKey().map(_._2.size).collect()
      val min1 = sizes1.min
      val max1 = sizes1.max
      val repart1 = max1.toDouble/min1
      val repartitioned: RDD[(Int, Iterable[HeatmapVertex])] = VariableSizeStripPartitioner.getPartitionsWithRelativeSizeSample(rdd,0.1).groupByKey()
      assert(repartitioned.keys.distinct().count()==4)
      val sizes = repartitioned.map(_._2.size)
      println(sizes.collect.mkString("\t"))
      val min = sizes.min
      val max = sizes.max
      sc.stop()
      println(max.toDouble/min)
      assert(min*3>max)
      val d = if(min==0)max.toDouble else (max.toDouble/min)
      //assert(repart1 > d)
  }
  */

  "fusionParts" must "fusionParts" in {
    var sizes: Array[Seq[Int]] = Array(Seq(11,12),Seq(12),Seq(4),Seq(14,13),Seq(3),Seq(4),Seq(11),Seq(15,16))
    val limits=Vector((-20.0,0),(-10.0,0),(0.0,1),(10.0,2),(20.0,3),(30.0,3),(40.0,4),(50.0,5),(60.0,6),(70.0,7),(Double.MaxValue,7))
    val result: Vector[(Double, Int)] = VariableSizeStripPartitioner.fusionParts(sizes,limits,10)
    val expectedResult  =Vector((-20.0,0),(-10.0,0),(0.0,1),(10.0,2),(20.0,3),(30.0,3),(50.0,4),(60.0,5),(70.0,6),(Double.MaxValue,6))
    println(result)
    println(expectedResult)
    assert(result.size==expectedResult.size)
    result.indices.foreach(i => {
      assert(result(i)==expectedResult(i))
    })

  }


}
