package fr.labri.speeddata.sparktxtimport

import fr.labri.speeddata.heatmapdata
import org.apache.spark.{SparkContext, SparkConf}
import fr.labri.speeddata.heatmapdata._
import org.apache.spark.rdd._
import org.apache.spark.SparkContext.{rddToSequenceFileRDDFunctions, rddToPairRDDFunctions}
/**
 * Created by alexandre on 11/06/14.
 */
object SparkTxtImport {

  def main(args: Array[String]) {

    val src = args(0)
    val dest = args(1)
    val nbPart: Int = if(args.size < 3) 30 else args(2).toInt
    val agg = if(args.  size < 4) true else args(3).toBoolean
    val separator: String = if(args.size<5) "\t" else if(args(4).equals("t")) "\t" else args(4)
    val fields: (Int, Int, Int) = if(args.size<8 ) (3,2,-1) else (args(5).toInt,args(6).toInt,args(7).toInt)
    val size: Int = if(args.size<9 ) 5 else args(8).toInt
    val conf = new SparkConf()
      //.setMaster("yarn-standalone")
      .setAppName("SparkCanopy")
      //.set("spark.executor.memory", "1g")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryo.registrator", "fr.labri.speeddata.sparkcanopy.HeatMapKryoRegistrator")
    val sc = new SparkContext(conf)
    val dataset = sc.textFile(src).repartition(nbPart)
    val datasetLines = dataset.map(_.split(separator)).filter(arr => arr.size == size && !arr(fields._1).exists(_.isLetter)).keyBy(arr => {

      new HeatmapVertex(MercatorProjection.getX(arr(fields._1).toDouble), MercatorProjection.getY(arr(fields._2).toDouble), 0)
    }).filter {
      case (v, _) => v.getX() > -180.0 && v.getX() < 180.0 && v.getY > -180.0 && v.getY < 180.0
    }.mapValues(arr => {
      if (fields._3 == -1 || arr(fields._3).size == 0)
        1
      else arr(fields._3).toLong
    })
      val heatmapData = if(agg) {
        datasetLines.reduceByKey(_ + _).map {
          case (v, pos) => {
            var outK = new HeatmapVertexWritableComparable()
            var outV = new PositionDataWritable()
            outK.set(v)
            outV.set(new heatmapdata.PositionData(pos))
            (outK, outV)
          }
        }
      }
    else{
      datasetLines.map {
          case (v, pos) => {
            var outK = new HeatmapVertexWritableComparable()
            var outV = new PositionDataWritable()
            outK.set(v)
            outV.set(new heatmapdata.PositionData(pos))
            (outK, outV)
          }
        }
      }
    heatmapData.saveAsSequenceFile(dest)

  }

  }
