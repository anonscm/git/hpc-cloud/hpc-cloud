To compile the project : `mvn package`

To run Canopy, Hadoop environment must be started. Then, start from
the project root directory
`yarn jar target/Canopy-1.1.1-SNAPSHOT.jar`
with expected arguments.

For example
`yarn jar target/Canopy-1.1.1-SNAPSHOT.jar -libjars target/Canopy-1.1.1-SNAPSHOT.jar /user/flalanne/CanopyData/brightkite_sequenceFile /user/$USER/result 5`

The option 'libjars' indicates the jar file to be deployed on the
remote nodes.

To look at the results file
`hdfs dfs -ls /user/$USER/result`
