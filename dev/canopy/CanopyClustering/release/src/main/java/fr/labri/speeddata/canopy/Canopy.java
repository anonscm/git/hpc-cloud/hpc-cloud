/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.canopy;

import fr.labri.speeddata.heatmapdata.HeatmapVertex;
import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.io.Writable;

/**
 *
 * @author alexandre
 */
public class Canopy implements Writable, Cloneable {

    private ArrayList<HeatmapVertex> dataPoints;
    private HeatmapVertex center;
    private long totalHits;
    private double d1;
    private double d2;

    public Canopy() {
        dataPoints = new ArrayList<>();
    }

    public Canopy(HeatmapVertex c, double t1, double t2) {
        assert (t1 <= t2);
        center = c;
        totalHits = c.getHits();
        d1 = t1;
        d2 = t2;
        dataPoints = new ArrayList<>();
    }

    public HeatmapVertex getCenter() {
        return center;
    }

    public Iterable<HeatmapVertex> getPoints() {
        return dataPoints;
    }

    public boolean isIn(HeatmapVertex v) {
        double xDist = Math.abs(v.getX() - center.getX());
        double yDist = Math.abs(v.getY() - center.getY());
        return Math.sqrt(xDist * xDist + yDist * yDist) <= d2;
    }

    public void add(HeatmapVertex v) {
        dataPoints.add(v);
    }
    
    public void addHits(long hits) {
        totalHits += hits;
    }
    
    public Long getTotalHits() {
        return totalHits;
    }

    @Override
    public void write(DataOutput d) throws IOException {
        d.writeDouble(d1);
        d.writeDouble(d2);
        HeatmapVertexWritableComparable w = new HeatmapVertexWritableComparable();
        w.set(center);
        w.write(d);
        d.writeLong(totalHits);
        d.writeInt(dataPoints.size());
        for (HeatmapVertex v : dataPoints) {
            w.set(v);
            w.write(d);
        }
    }

    @Override
    public void readFields(DataInput di) throws IOException {
        d1 = di.readDouble();
        d2 = di.readDouble();
        HeatmapVertexWritableComparable w = new HeatmapVertexWritableComparable();
        w.readFields(di);
        center = w.get();
        totalHits = di.readLong();
        int size = di.readInt();
        dataPoints.ensureCapacity(size);
        for (int i = 0; i < size; i++) {
            w.readFields(di);
            dataPoints.add(w.get());
        }
    }

    @Override
    public Canopy clone() {
        try {
            return (Canopy) super.clone(); //To change body of generated methods, choose Tools | Templates.
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Canopy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
}
