/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.canopy;

import fr.labri.speeddata.heatmapdata.HeatmapVertex;
import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

/**
 *
 * @author alexandre
 */
public class HeatmapVertexWithType implements Writable, Serializable{

    public HeatmapVertex vertex;
    public PointType type;

    public HeatmapVertexWithType() {
    }

    public HeatmapVertexWithType(HeatmapVertex vertex, PointType type) {
        this.vertex = vertex;
        this.type = type;
    }
    
    @Override
    public void write(DataOutput d) throws IOException {
        WritableUtils.writeEnum(d, type);
        HeatmapVertexWritableComparable w = new HeatmapVertexWritableComparable();
        w.set(vertex);
        w.write(d);
    }

    @Override
    public void readFields(DataInput di) throws IOException {
        type = WritableUtils.readEnum(di, PointType.class);
        HeatmapVertexWritableComparable w = new HeatmapVertexWritableComparable();
        w.readFields(di);
        vertex = w.get();
    }
    
}
