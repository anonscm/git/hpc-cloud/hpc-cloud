/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.canopy;

import static fr.labri.speeddata.canopy.CanopyMapper.D1_KEY;
import static fr.labri.speeddata.canopy.CanopyMapper.D2_KEY;
import fr.labri.speeddata.heatmapdata.HeatmapVertex;
import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import fr.labri.speeddata.heatmapdata.PositionData;
import fr.labri.speeddata.heatmapdata.PositionDataWritable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author alexandre
 */
public class CanopyReducer extends Reducer<NullWritable, Canopy, HeatmapVertexWritableComparable, PositionDataWritable> {
    private double d1;
    private double d2;

    private ArrayList<Canopy> canopies;
    private LinkedList<HeatmapVertex> orphanVertices;
    
    @Override
    protected void reduce(NullWritable key, Iterable<Canopy> values, Context context) throws IOException, InterruptedException {
        //get independant canopies
        for (Canopy c1 : values) {
            boolean redundant = false;
            for (Canopy c2 : canopies) {
                if(c2.isIn(c1.getCenter())) {
                    System.out.println(c1.getCenter()+" is inside canopy with center "+c2.getCenter());
                    context.getCounter(CanopyCounters.REDUCER_REDUNDANT_CENTERS).increment(1);
                    context.getCounter(CanopyCounters.REDUCER_ORPHAN_NODES).increment(1);
                    orphanVertices.add(c1.getCenter());
                    for(HeatmapVertex v : c1.getPoints()) {
                        context.getCounter(CanopyCounters.REDUCER_ORPHAN_NODES).increment(1);
                        orphanVertices.add(v);
                    }
                    redundant = true;
                    break;
                }
            }
            if(!redundant) {
                context.getCounter(CanopyCounters.REDUCER_CONFIRMED_CANOPIES).increment(1);
                canopies.add(c1.clone());
            }
        }
        
        //assign orphan vertices
        for(HeatmapVertex v : orphanVertices) {
            boolean added = false;
            for (Canopy c : canopies) {
                if(c.isIn(v)) {
                    context.getCounter(CanopyCounters.REDUCER_MERGED_ORPHAN).increment(1);
                    c.add(v);
                    added = true;
                    break;
                }   
            }
            if(!added) {
                context.getCounter(CanopyCounters.REDUCER_NEW_CANOPIES).increment(1);
                canopies.add(new Canopy(v, d1, d2));
            }
        }
        
        //aggregate canopies
        HeatmapVertexWritableComparable outK = new HeatmapVertexWritableComparable();
        PositionDataWritable outV = new PositionDataWritable();
        for (Canopy c : canopies) {
            HeatmapVertex center = c.getCenter();
            outK.set(center);
            long hits = c.getTotalHits();
           
            outV.set(new PositionData(hits));
            
            context.write(outK, outV);
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        d1 = context.getConfiguration().getDouble(D1_KEY, 1);
        d2 = context.getConfiguration().getDouble(D2_KEY, 1);
        canopies = new ArrayList<>();
        orphanVertices = new LinkedList<>();
    }
    
    
}
