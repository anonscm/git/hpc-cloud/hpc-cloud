/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.heatmapdata;

import java.io.Serializable;

/**
 *
 * @author alexandre
 */
public class HeatmapVertex implements Comparable<HeatmapVertex>, Serializable {

    private double x;
    private double y;
    private long hits;

    public HeatmapVertex(double x, double y) {
        this.x = x;
        this.y = y;
        this.hits = 0;
    }

    public HeatmapVertex(double x, double y, long hits) {
        this.x = x;
        this.y = y;
        this.hits = hits;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public long getHits() {
        return hits;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof HeatmapVertex)) {
            return false;
        } else {
            HeatmapVertex other = (HeatmapVertex) obj;
            return Math.abs(this.x - other.x) <= 0.000001 && Math.abs(this.y - other.y) <= 0.000001;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        return hash;
    }

    @Override
    public String toString() {
        return x + " " + y + " " + hits;
    }

    @Override
    public int compareTo(HeatmapVertex t) {
        double diff = this.x - t.x;
        if (diff > 0.000001) {
            return 1;
        } else if (diff < -0.000001) {
            return -1;
        }

        diff = this.y - t.y;
        if (diff > 0.000001) {
            return 1;
        } else if (diff < -0.000001) {
            return -1;
        }
        
        return 0;
    }

}
