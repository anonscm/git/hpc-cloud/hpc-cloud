/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.canopy;

import com.github.varunpant.quadtree.Point;
import com.github.varunpant.quadtree.QuadTree;
import fr.labri.speeddata.heatmapdata.HeatmapVertex;
import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author alexandre
 */
public class CanopyStripSecondPassReducer extends Reducer<IntWritable, HeatmapVertexWithType, HeatmapVertexWritableComparable, HeatmapVertexWritableComparable> {

    private HeatmapVertexWritableComparable outK;
    private HeatmapVertexWritableComparable outV;

    private int pass;
    private double strip_length;
    private double dist;

    private QuadTree<Canopy> canopies;

    @Override
    protected void reduce(IntWritable key, Iterable<HeatmapVertexWithType> values, Context context) throws IOException, InterruptedException {
        ArrayList<HeatmapVertex> centers = new ArrayList<>();
        ArrayList<HeatmapVertex> points = new ArrayList<>();
        canopies = new QuadTree<>(-180,-180,180,180);
        for (HeatmapVertexWithType v : values) {
            switch (v.type) {
                case CENTER:
                    double y = v.vertex.getY();
                    double cdist = (y+180.) % strip_length;
                    if (cdist < dist || cdist > 2 * dist) {
                        context.getCounter(CanopyCounters.REDUCER_CONFIRMED_CANOPIES).increment(1);
                        for (Point<Canopy> p : canopies.searchWithin(v.vertex.getX()-dist, v.vertex.getY()-dist, v.vertex.getX()+dist, v.vertex.getY()+dist))
                            if(p.getValue().isIn(v.vertex)) context.getCounter("checks", "unresolved conflict from phase 1").increment(1);
                        canopies.set(v.vertex.getX(), v.vertex.getY(), new Canopy(v.vertex, dist, dist));
                    } else {
                        points.add(v.vertex);
                    }
                    break;
                case POINT:
                    points.add(v.vertex);
                    break;
            }
        }
        //Resolve center conflicts
        for (HeatmapVertex v : centers) {
            boolean covered = false;
            for (Point<Canopy> p : canopies.searchWithin(v.getX()-dist, v.getY()-dist, v.getX()+dist, v.getY()+dist)) {
                if (p.getValue().isIn(v)) {
                    p.getValue().add(v);
                    covered = true;
                    context.getCounter(CanopyCounters.REDUCER_REDUNDANT_CENTERS).increment(1);
                    break;
                }
            }
            if (!covered) {
                context.getCounter(CanopyCounters.REDUCER_NEW_CANOPIES).increment(1);
                canopies.set(v.getX(), v.getY(), new Canopy(v, dist, dist));
            }
        }
        QuadTree<Canopy> newCanopies = new QuadTree<>(-180,-180, 180, 180);
        for (HeatmapVertex v : points) {
            boolean covered = false;
            boolean passed = false;
            for (Point<Canopy> p : canopies.searchWithin(v.getX()-dist, v.getY()-dist, v.getX()+dist, v.getY()+dist)) {
                passed = true;
                System.out.println("comparing : "+v + " and "+p.getValue().getCenter());
                if (p.getValue().isIn(v)) {
                    System.out.println("covered");
                    p.getValue().add(v);
                    covered = true;
                    context.getCounter(CanopyCounters.REDUCER_MERGED_ORPHAN).increment(1);
                    break;
                }
            }
            for (Point<Canopy> p : newCanopies.searchWithin(v.getX()-dist, v.getY()-dist, v.getX()+dist, v.getY()+dist)) {
                passed = true;
                if (p.getValue().isIn(v)) {
                    p.getValue().add(v);
                    covered = true;
                    context.getCounter(CanopyCounters.REDUCER_MERGED_ORPHAN).increment(1);
                    break;
                }
            }
            if (!covered) {
                if(!passed) context.getCounter("checks", "no canopy at dist").increment(1);
                context.getCounter(CanopyCounters.REDUCER_ORPHAN_NODES).increment(1);
                //if(passed)
                    newCanopies.set(v.getX(), v.getY(), new Canopy(v, dist, dist));
            }
        }

        writeCanopies(context,canopies.getValues());
        writeCanopies(context, newCanopies.getValues());
    }

    private void writeCanopies(Context context, Iterable<Canopy> canopies) throws IOException, InterruptedException {
        HeatmapVertexWritableComparable dummy = new HeatmapVertexWritableComparable();
        dummy.set(new HeatmapVertex(0, 0, 0));
        for (Canopy c : canopies) {
            outK.set(c.getCenter());
            //write dummy vertex to avoid losing canopies with only their center
            context.write(outK, dummy);
            for (HeatmapVertex v : c.getPoints()) {
                outV.set(v);
                context.write(outK, outV);
            }
        }
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        strip_length = context.getConfiguration().getDouble(CanopyConfigKeys.STRIP_LENGTH_KEY, 1.0);
        dist = context.getConfiguration().getDouble(CanopyConfigKeys.CANOPY_DIST_KEY, 1.0);
        pass = context.getConfiguration().getInt(CanopyConfigKeys.PASS_KEY, 1);

        outK = new HeatmapVertexWritableComparable();
        outV = new HeatmapVertexWritableComparable();
    }
}
