/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.canopy;

import com.github.varunpant.quadtree.Point;
import com.github.varunpant.quadtree.QuadTree;
import fr.labri.speeddata.heatmapdata.HeatmapVertex;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author alexandre
 */
public class CanopyStripReducer extends Reducer<IntWritable, HeatmapVertexWithType, NullWritable, HeatmapVertexWithType> {

    private int pass;
    private double strip_length;
    private double dist;

    private QuadTree<Canopy> canopies;
    private ArrayList<HeatmapVertex> vertices;

    @Override
    protected void reduce(IntWritable key, Iterable<HeatmapVertexWithType> values, Context context) throws IOException, InterruptedException {
        canopies = new QuadTree<>(-180, -180, 180, 180);
        vertices.clear();
        NullWritable outK = NullWritable.get();
        HeatmapVertexWithType outV = new HeatmapVertexWithType();
        for (HeatmapVertexWithType v : values) {
            switch (v.type) {
                case CENTER:
                    boolean redundant = false;
                    for (Point<Canopy> p : canopies.searchWithin(v.vertex.getX() - dist, v.vertex.getY() - dist, v.vertex.getX() + dist, v.vertex.getY() + dist)) {
                        if (p.getValue().isIn(v.vertex)) {
                            //context.getCounter("canopies", "points in strip " + key.get()).increment(1);
                            context.getCounter(CanopyCounters.REDUCER_REDUNDANT_CENTERS).increment(1);
                            outV.vertex = v.vertex;
                            outV.type = PointType.POINT;
                            context.write(outK, outV);
                            redundant = true;
                            break;
                        }
                    }
                    if (!redundant) {
                        //context.getCounter("canopies", "canopies in strip " + key.get()).increment(1);
                        canopies.set(v.vertex.getX(), v.vertex.getY(), new Canopy(v.vertex, dist, dist));
                    }
                    break;
                case POINT:
                    //context.getCounter("canopies", "points in strip " + key.get()).increment(1);
                    context.getCounter(CanopyCounters.REDUCER_ORPHAN_NODES).increment(1);
                    redundant = false;
                    for (Point<Canopy> p : canopies.searchWithin(v.vertex.getX() - dist, v.vertex.getY() - dist, v.vertex.getX() + dist, v.vertex.getY() + dist)) {
                        if (p.getValue().isIn(v.vertex)) {
                            //context.getCounter("canopies", "points in strip " + key.get()).increment(1);
                            context.getCounter(CanopyCounters.REDUCER_MERGED_ORPHAN).increment(1);
                            outV.vertex = v.vertex;
                            outV.type = PointType.POINT;
                            context.write(outK, outV);
                            redundant = true;
                            break;
                        }
                    }
                    if (!redundant) {
                        //context.getCounter("canopies", "canopies in strip " + key.get()).increment(1);
                        vertices.add(v.vertex);
                    }
                    break;
            }
        }

        for (HeatmapVertex v : vertices) {
            boolean covered = false;
            for (Point<Canopy> p : canopies.searchWithin(v.getX() - dist, v.getY() - dist, v.getX() + dist, v.getY() + dist)) {
                if (p.getValue().isIn(v)) {
                    covered = true;
                    outV.vertex = v;
                    outV.type = PointType.POINT;
                    context.write(outK, outV);
                    break;
                }
            }
            if(!covered) {
                canopies.set(v.getX(), v.getY(), new Canopy(v, dist, dist));
            }
        }

        for (Canopy c : canopies.getValues()) {
            outV.vertex = c.getCenter();
            outV.type = PointType.CENTER;
            context.write(outK, outV);
        }
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        strip_length = context.getConfiguration().getDouble(CanopyConfigKeys.STRIP_LENGTH_KEY, 1.0);
        dist = context.getConfiguration().getDouble(CanopyConfigKeys.CANOPY_DIST_KEY, 1.0);
        pass = context.getConfiguration().getInt(CanopyConfigKeys.PASS_KEY, 0);

        vertices = new ArrayList<>();
    }

}
