/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.canopy;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author alexandre
 */
public class PointToStripSecondPassMapper extends Mapper<NullWritable, HeatmapVertexWithType, IntWritable, HeatmapVertexWithType> {

    private double strip_length;
    private double dist;
    private int pass;

    private StripCalculator stripCalc;

    private HeatmapVertexWithType outV;
    private IntWritable outK;

    @Override
    protected void map(NullWritable key, HeatmapVertexWithType value, Context context) throws IOException, InterruptedException {
        if (value.type == PointType.POINT) {
            outK.set((int)Math.floor((value.vertex.getY()+180.)/strip_length));
            System.out.println("point with y="+value.vertex.getY()+"mapped to strip "+outK.get());
            context.write(outK, value);
        }
        else if (value.type == PointType.CENTER) {
            double y = value.vertex.getY();
            double cdist = (y+180.)%strip_length; //avoid negative modulo
            int strip = (int) Math.floor((y+180)/strip_length);
            outK.set(strip);
            context.write(outK, value);
            System.out.println("for y="+y+", map to strip "+strip+", with cdist="+cdist);
            if (cdist <= dist) {
                outK.set(strip - 1);
                context.write(outK, value);
                System.out.println("also write to strip "+(strip-1));
            }
            if (cdist >= strip_length - dist) {
                outK.set(strip + 1);
                context.write(outK, value);
                System.out.println("also write to strip "+(strip+1));
            }
        }
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        strip_length = context.getConfiguration().getDouble(CanopyConfigKeys.STRIP_LENGTH_KEY, 1.0);
        dist = context.getConfiguration().getDouble(CanopyConfigKeys.CANOPY_DIST_KEY, 1.0);
        pass = context.getConfiguration().getInt(CanopyConfigKeys.PASS_KEY, 1);

        outV = new HeatmapVertexWithType();
        outK = new IntWritable();

        stripCalc = new StripCalculator(pass, strip_length, dist);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {

    }

}
