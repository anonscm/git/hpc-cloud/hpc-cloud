/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.canopy;

import com.github.varunpant.quadtree.Point;
import com.github.varunpant.quadtree.QuadTree;
import fr.labri.speeddata.heatmapdata.HeatmapVertex;
import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import fr.labri.speeddata.heatmapdata.PositionDataWritable;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author alexandre
 */
public class PointToStripMapper extends Mapper<HeatmapVertexWritableComparable, PositionDataWritable, IntWritable, HeatmapVertexWithType>{
    
    private double strip_length;
    private double dist;
    private int pass;

    private StripCalculator stripCalc;
    
    private QuadTree<Canopy> canopies;
    private HeatmapVertexWithType outV;
    private IntWritable outK;
    
    @Override
    protected void map(HeatmapVertexWritableComparable key, PositionDataWritable value, Context context) throws IOException, InterruptedException {
        HeatmapVertex tmp = key.get();
        HeatmapVertex v = new HeatmapVertex(tmp.getX(), tmp.getY(), value.get().getNbPages());
        
        for(Point<Canopy> p : canopies.searchWithin(v.getX()-dist, v.getY()-dist, v.getX()+dist, v.getY()+dist)) {
            Canopy c = p.getValue();
            if(c.isIn(v)) {
                context.getCounter(CanopyCounters.MAPPER_MERGED_VERTICES).increment(1);
                outV.vertex = v;
                outV.type = PointType.POINT;
                outK.set(stripCalc.pointToStrip(v));
                context.write(outK, outV);
                return;
            }
        }
        
        canopies.set(v.getX(), v.getY(), new Canopy(v, dist, dist));
        context.getCounter(CanopyCounters.MAPPER_CANOPIES).increment(1);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        strip_length = context.getConfiguration().getDouble(CanopyConfigKeys.STRIP_LENGTH_KEY, 1.0);
        dist = context.getConfiguration().getDouble(CanopyConfigKeys.CANOPY_DIST_KEY, 1.0);
        pass = context.getConfiguration().getInt(CanopyConfigKeys.PASS_KEY, 1);
        
        canopies = new QuadTree<>(-180, -180, 180, 180);
        outV = new HeatmapVertexWithType();
        outK = new IntWritable();
        
        stripCalc = new StripCalculator(pass, strip_length, dist);
    }
    
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        outV.type = PointType.CENTER;
        for(Canopy c : canopies.getValues()) {
            outV.vertex = c.getCenter();
            outK.set(stripCalc.pointToStrip(c.getCenter()));
            context.write(outK, outV);
        }
    }
  
}
