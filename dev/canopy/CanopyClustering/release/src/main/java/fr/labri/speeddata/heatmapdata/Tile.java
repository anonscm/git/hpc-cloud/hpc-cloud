/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.heatmapdata;

/**
 *
 * @author aperrot
 */
public class Tile implements Comparable<Tile>{
    
    public static Tile getTile(LatLng pos, int level) {
        double x = MercatorProjection.getX(pos.getLongitude());
        double y = MercatorProjection.getY(pos.getLatitude());
        return getTile(x, y, level);
    }
    
    public static Tile getTile(HeatmapVertex vertex, int level) {
        return getTile(vertex.getX(), vertex.getY(), level);
    }
    
    private static Tile getTile(double x, double y, int level) {
        double dist = 360.0/Math.pow(2,level);
        return new Tile((int)Math.floor((x+180)/dist), (int)Math.floor((y+180)/dist));
    }
    
    private int x;
    private int y;
    
    public Tile(int tileX, int tileY) {
        this.x=tileX;
        this.y=tileY;
    }

    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    @Override
    public String toString() {
        return x+" "+y;
    }
    
    public int compareTo(Tile t) {
        if(t==null) return 1;
        if(this.x != t.x)
            return this.x-t.x;
        else
            return this.y-t.y;
    }

    @Override
    public int hashCode() {
        return (x+y)*(x+y+1)/2 + y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tile other = (Tile) obj;
        if (this.x != other.x) {
            return false;
        }
        return this.y == other.y;
    }
    
    
}
