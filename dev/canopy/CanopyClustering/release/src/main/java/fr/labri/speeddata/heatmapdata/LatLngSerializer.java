/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.heatmapdata;

//import fr.labri.speeddata.common.io.Serializer;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 *
 * @author flalanne
 */
public enum LatLngSerializer /*implements Serializer<LatLng> */{
    INSTANCE;

    public void serialize(DataOutput dataOutput, LatLng instance) throws IOException {
        dataOutput.writeDouble(instance.getLongitude());
        dataOutput.writeDouble(instance.getLatitude());
    }

    public LatLng deserialize(DataInput dataInput) throws IOException {
        double longitude = dataInput.readDouble();
        double latitude = dataInput.readDouble();
        return new LatLng(latitude, longitude);
    }
    
}
