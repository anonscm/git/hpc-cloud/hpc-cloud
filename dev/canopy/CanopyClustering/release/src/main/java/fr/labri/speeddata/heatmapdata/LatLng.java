/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.heatmapdata;

/**
 *
 * @author flalanne
 */
public class LatLng {
    private double latitude;
    private double longitude;
    
    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public LatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    
    

    int compareTo(LatLng p) {
        double l=longitude-p.longitude;
        if(l<0)
            return -1;
        if(l>0)
            return 1;
        l= latitude-p.latitude;
        if(l<0)
            return -1;
        if(l>0)
            return 1;
        return 0;
        
    }
    
    @Override
    public String toString() {
        return longitude + " " + latitude;
    }
    
}
