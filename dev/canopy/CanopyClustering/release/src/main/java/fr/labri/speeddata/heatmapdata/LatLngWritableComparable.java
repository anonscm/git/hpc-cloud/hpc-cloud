/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.heatmapdata;

//import fr.labri.speeddata.common.io.Serializer;
//import fr.labri.speeddata.common.io.hadoop.WritableSerializerWrapper;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.io.WritableComparable;

/**
 *
 * @author flalanne
 */
public class LatLngWritableComparable /*extends WritableSerializerWrapper<LatLng>*/ implements WritableComparable<LatLngWritableComparable> {
    private LatLng value;

    public LatLng get() {
        return value;
    }

    public void set(LatLng value) {
        this.value = value;
    }
    
//    
//    @Override
//    protected Serializer<LatLng> getSerializer(DataOutput dataOutput, LatLng value) throws IOException {
//        return LatLngSerializer.INSTANCE;
//    }
//
//    @Override
//    protected Serializer<LatLng> getSerializer(DataInput dataInput) throws IOException {
//        return LatLngSerializer.INSTANCE;
//    }

    public int compareTo(LatLngWritableComparable o) {
        return get().compareTo(o.get());
    }

    
    /* 
    TODO
    improve hashing
    */
    @Override
    public int hashCode() {
        //return (int) (Double.doubleToLongBits(get().getLongitude())^Double.doubleToLongBits(get().getLatitude()));/*
        
        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] input = new byte[16];
            ByteBuffer b = ByteBuffer.wrap(input);
            b.putDouble(get().getLatitude());
            b.putDouble(get().getLongitude());
            md.update(input);
            byte[] digest = md.digest();
            md.reset();
            Integer i;
            ByteBuffer wrap = ByteBuffer.wrap(digest);
            return  wrap.getInt();
            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(LatLngWritableComparable.class.getName()).log(Level.SEVERE, null, ex);
            return (int) (Double.doubleToLongBits(get().getLongitude())^Double.doubleToLongBits(get().getLatitude()));
        }
                
    }
    
    @Override
    public String toString() {
        return get().toString();
    }

    @Override
    public void write(DataOutput out) throws IOException {
        LatLngSerializer.INSTANCE.serialize(out, value);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        set(LatLngSerializer.INSTANCE.deserialize(in));
    }
    
    
    
}
