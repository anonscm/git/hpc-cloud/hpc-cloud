/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.canopy;

import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import fr.labri.speeddata.heatmapdata.PositionDataWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import java.util.Random;

/**
 *
 * @author alexandre
 */
public class CanopyJob extends Configured implements Tool {

    static final String JOB_NAME = "CanopyClustering";
    static Random rand = new Random();

    @Override
    public int run(String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println(JOB_NAME + " : inputPath outputPath level");
            System.exit(0);
        }

        String input = args[0];
        String output = args[1];
        double level = Double.parseDouble(args[2]);
        double dist = 360.0/(10*Math.sqrt(10)*Math.pow(2,level));
        double d1 = dist/2;
        double d2 = dist;
        
        System.out.println(JOB_NAME + " for level "+level+". D1="+d1+", D2="+d2);
	String username = org.apache.hadoop.security.UserGroupInformation.getCurrentUser().getShortUserName();
        int n = rand.nextInt(50);
	String tmpoutput1 = "/tmp/"+username+"/canopy/" + String.valueOf(n) + "/phase1/";
        String tmpoutput2 = "/tmp/"+username+"/canopy/" + String.valueOf(n) + "/phase2/";
        
        Configuration conf = getConf();
        conf.setDouble("mapreduce.job.reduce.slowstart.completedmaps", 0.9);
        conf.setInt("mapreduce.map.memory.mb", 3072);
        conf.set("mapred.child.java.opts", "-Xmx2048m");
        conf.setInt("mapreduce.reduce.memory.mb", 3072);
        conf.set("mapred.child.java.opts", "-Xmx2048m");
        conf.setDouble("yarn.nodemanager.vmem-pmem-ratio", 3.0);
        FileSystem hdfs = FileSystem.get(conf);

        int reduces = args.length > 3 ? Integer.parseInt(args[3]) : 16;
        
        double strip_length =4*d2;
        int numReduce = (int) (360/strip_length);
        
        System.out.println("Length of a strip : "+strip_length+", number of strips : "+numReduce);
        
        Job canopyJob = new Job(conf, JOB_NAME + "phase 1");
        canopyJob.getConfiguration().setDouble(CanopyMapper.D1_KEY, d1);
        canopyJob.getConfiguration().setDouble(CanopyMapper.D2_KEY, d2);
        canopyJob.getConfiguration().setDouble(CanopyConfigKeys.CANOPY_DIST_KEY, d2);
        canopyJob.getConfiguration().setInt(CanopyConfigKeys.PASS_KEY, 0);
        canopyJob.getConfiguration().setDouble(CanopyConfigKeys.STRIP_LENGTH_KEY, strip_length);
        canopyJob.setNumReduceTasks(reduces);
        canopyJob.setReduceSpeculativeExecution(false);
        
        //Input
        canopyJob.setInputFormatClass(SequenceFileInputFormat.class);
        SequenceFileInputFormat.setInputPaths(canopyJob, new Path(input));
        SequenceFileInputFormat.setMaxInputSplitSize(canopyJob, SPLIT_SIZE);
        SequenceFileInputFormat.setMinInputSplitSize(canopyJob, SPLIT_SIZE);

        //Mapper
        canopyJob.setMapperClass(PointToStripMapper.class);
        canopyJob.setMapOutputKeyClass(IntWritable.class);
        canopyJob.setMapOutputValueClass(HeatmapVertexWithType.class);

        //Reducer
        canopyJob.setReducerClass(CanopyStripReducer.class);

        //Output
        canopyJob.setOutputFormatClass(SequenceFileOutputFormat.class);
        canopyJob.setOutputKeyClass(NullWritable.class);
        canopyJob.setOutputValueClass(HeatmapVertexWithType.class);
        SequenceFileOutputFormat.setOutputPath(canopyJob, new Path(tmpoutput1));

        canopyJob.waitForCompletion(true);

        
        canopyJob = new Job(conf, JOB_NAME + "phase 2");
        canopyJob.getConfiguration().setDouble(CanopyMapper.D1_KEY, d1);
        canopyJob.getConfiguration().setDouble(CanopyMapper.D2_KEY, d2);
        canopyJob.getConfiguration().setDouble(CanopyConfigKeys.CANOPY_DIST_KEY, d2);
        canopyJob.getConfiguration().setInt(CanopyConfigKeys.PASS_KEY, 1);
        canopyJob.getConfiguration().setDouble(CanopyConfigKeys.STRIP_LENGTH_KEY, strip_length);
        canopyJob.setNumReduceTasks(reduces);
        canopyJob.setReduceSpeculativeExecution(false);

        //Input
        canopyJob.setInputFormatClass(SequenceFileInputFormat.class);
        SequenceFileInputFormat.setInputPaths(canopyJob, new Path(tmpoutput1));

        //Mapper
        canopyJob.setMapperClass(PointToStripSecondPassMapper.class);
        canopyJob.setMapOutputKeyClass(IntWritable.class);
        canopyJob.setMapOutputValueClass(HeatmapVertexWithType.class);

        //Reducer
        canopyJob.setReducerClass(CanopyStripSecondPassReducer.class);

        //Output
        canopyJob.setOutputFormatClass(SequenceFileOutputFormat.class);
        canopyJob.setOutputKeyClass(HeatmapVertexWritableComparable.class);
        canopyJob.setOutputValueClass(HeatmapVertexWritableComparable.class);
        SequenceFileOutputFormat.setOutputPath(canopyJob, new Path(tmpoutput2));

        hdfs.deleteOnExit(new Path(tmpoutput1));
        canopyJob.waitForCompletion(true);
        
        
        canopyJob = new Job(conf, JOB_NAME + " sum");
        canopyJob.setNumReduceTasks(reduces);
        canopyJob.setReduceSpeculativeExecution(false);

        //Input
        canopyJob.setInputFormatClass(SequenceFileInputFormat.class);
        SequenceFileInputFormat.setInputPaths(canopyJob, new Path(tmpoutput2));

        //Map
        canopyJob.setMapOutputKeyClass(HeatmapVertexWritableComparable.class);
        canopyJob.setMapOutputValueClass(HeatmapVertexWritableComparable.class);
        
        //Reducer
        canopyJob.setReducerClass(CanopySumReducer.class);

        //Output
        canopyJob.setOutputFormatClass(SequenceFileOutputFormat.class);
        canopyJob.setOutputKeyClass(HeatmapVertexWritableComparable.class);
        canopyJob.setOutputValueClass(PositionDataWritable.class);
        SequenceFileOutputFormat.setOutputPath(canopyJob, new Path(output));
        SequenceFileOutputFormat.setOutputCompressorClass(canopyJob, DefaultCodec.class);

        hdfs.deleteOnExit(new Path(tmpoutput2));
        canopyJob.waitForCompletion(true);
        
        
        return 0;
    }
    private static final int SPLIT_SIZE = 64*1024*1024;

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new CanopyJob(), args));
    }

}
