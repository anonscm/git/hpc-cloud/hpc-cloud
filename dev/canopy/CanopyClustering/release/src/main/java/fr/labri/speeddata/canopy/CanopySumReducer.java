/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.canopy;

import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import fr.labri.speeddata.heatmapdata.PositionData;
import fr.labri.speeddata.heatmapdata.PositionDataWritable;
import java.io.IOException;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author alexandre
 */
public class CanopySumReducer extends Reducer<HeatmapVertexWritableComparable, HeatmapVertexWritableComparable, HeatmapVertexWritableComparable, PositionDataWritable> {

    private PositionDataWritable outV;
    
    @Override
    protected void reduce(HeatmapVertexWritableComparable key, Iterable<HeatmapVertexWritableComparable> values, Context context) throws IOException, InterruptedException {
        long hits = key.get().getHits();
        for (HeatmapVertexWritableComparable v : values) {
            hits += v.get().getHits();
        }
        outV.set(new PositionData(hits));
        context.write(key, outV);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        outV = new PositionDataWritable();
    }
    
    
    
}
