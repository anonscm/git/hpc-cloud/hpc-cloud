/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.heatmapdata;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

/**
 *
 * @author remi
 */
public class SweepKey implements WritableComparable<SweepKey> {
    private int band;
    private Double x;
    
    /**
     * Constructor.
     */
    public SweepKey() { }
    
    /**
     * Constructor.
     */
    public SweepKey(int band, Double x) {
        this.band = band;
        this.x = x;
    }
    
    
    
    @Override
    public void readFields(DataInput in) throws IOException {
        band = in.readInt();
        x = in.readDouble();
    }
    
    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(band);
        out.writeDouble(x);
    }
    
    @Override
    public int compareTo(SweepKey sk) {
        int result = new Integer(band).compareTo(sk.band);
        if(result == 0) {
            result = x.compareTo(sk.x);
        }
        return result;
    }
    
    /**
     * Gets the band.
     * @return Band.
     */
    public int getBand() {
        return band;
    }
    
    public void setBand(int band) {
        this.band = band;
    }
    
    /**
     * Gets the x.
     * @return x.
     */
    public Double getX() {
        return x;
    }
    
    public void setX(Double x) {
        this.x = x;
    }
}
