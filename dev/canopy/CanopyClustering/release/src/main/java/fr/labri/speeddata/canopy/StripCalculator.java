/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.labri.speeddata.canopy;

import fr.labri.speeddata.heatmapdata.HeatmapVertex;

/**
 *
 * @author alexandre
 */
public class StripCalculator {

    private int pass;
    private double strip_length;
    private double dist;

    public StripCalculator(int pass, double strip_length, double dist) {
        this.pass = pass;
        this.strip_length = strip_length;
        this.dist = dist;
    }

    public int pointToStrip(HeatmapVertex v) {
        if (v.getY()+180. < dist) {
            return 0;
        }
        if (v.getY()+180. > 360. - dist) {
            return (int) Math.floor(360 / strip_length);
        }

        return (int) Math.floor((v.getY()+180. - dist) / strip_length);

        //unused
        /*
         if(pass%2 == 0) {
         return (int)(Math.floor(v.getY()/strip_length)) + stripBaseOffset(v.getY()) - stripBaseOffset(v.getY()-strip_length-dist);
         }
         else
         return (int)(Math.floor(v.getY()/strip_length) + stripBaseOffset(v.getY()+strip_length) - stripBaseOffset(v.getY()-dist));
         */
    }

    // returns 0 if the point is in its original strip, -1 for the strip before and 1 for the strip after
    private int stripBaseOffset(double x) {
        return (int) (Math.floor((x + dist) / (2 * strip_length)) - Math.floor(x / (2 * strip_length)));
    }

}
