/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.heatmapdata;

//import fr.labri.speeddata.common.io.Serializer;
//import fr.labri.speeddata.common.io.hadoop.WritableSerializerWrapper;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.WritableComparable;

/**
 *
 * @author alexandre
 */
public class HeatmapVertexWritableComparable /*extends WritableSerializerWrapper<HeatmapVertex>*/ implements WritableComparable<HeatmapVertexWritableComparable>{
    private HeatmapVertex value;

    public void set(HeatmapVertex value) {
        this.value = value;
    }
//    @Override
//    protected Serializer<HeatmapVertex> getSerializer(DataOutput d, HeatmapVertex t) throws IOException {
//        return HeatmapVertexSerializer.INSTANCE;
//    }
//
//    @Override
//    protected Serializer<HeatmapVertex> getSerializer(DataInput di) throws IOException {
//        return HeatmapVertexSerializer.INSTANCE;
//    }
    public HeatmapVertex get(){
        return value;
    }

    @Override
    public String toString() {
        return get().toString(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(HeatmapVertexWritableComparable t) {
        return this.get().compareTo(t.get());
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof HeatmapVertexWritableComparable))
            return false;
        else {
            HeatmapVertexWritableComparable other = (HeatmapVertexWritableComparable) obj;
            return this.get().equals(other.get());
        }
    }

    @Override
    public int hashCode() {
        return this.get().hashCode();
    }

    @Override
    public void write(DataOutput out) throws IOException {
        HeatmapVertexSerializer.INSTANCE.serialize(out, value);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        value = HeatmapVertexSerializer.INSTANCE.deserialize(in);
    }
    
    
    
    
}
