/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    COPYRIGHT Speeddata Project 2015
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.labri.speeddata.canopy;

import fr.labri.speeddata.heatmapdata.HeatmapVertex;
import fr.labri.speeddata.heatmapdata.HeatmapVertexWritableComparable;
import fr.labri.speeddata.heatmapdata.PositionDataWritable;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author alexandre
 */
public class CanopyMapper extends Mapper<HeatmapVertexWritableComparable, PositionDataWritable, NullWritable, Canopy>{

    static final String D1_KEY = "fr.labri.speeddata.canopy.d1";
    static final String D2_KEY = "fr.labri.speeddata.canopy.d2";
    
    private double d1;
    private double d2;
    
    private ArrayList<Canopy> canopies;
    
    @Override
    protected void map(HeatmapVertexWritableComparable key, PositionDataWritable value, Context context) throws IOException, InterruptedException {
        HeatmapVertex v = key.get();
        HeatmapVertex v2 = new HeatmapVertex(v.getX(), v.getY(), value.get().getNbPages());
        
        for(Canopy c : canopies) {
            if(c.isIn(v2)) {
                c.addHits(v2.getHits());
                context.getCounter(CanopyCounters.MAPPER_MERGED_VERTICES).increment(1);
                return;
            }
        }
        
        canopies.add(new Canopy(v2, d1, d2));
        context.getCounter(CanopyCounters.MAPPER_CANOPIES).increment(1);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        NullWritable n = NullWritable.get();
        for(Canopy c : canopies) {
            context.write(n, c);
        }
    }
    
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        d1 = context.getConfiguration().getDouble(D1_KEY, 1);
        d2 = context.getConfiguration().getDouble(D2_KEY, 1);
        canopies = new ArrayList<>();
    }

    
    
}
