#include <stdio.h>
#include <stdlib.h>
#include <starpu.h>

#define FPRINTF(ofile, fmt, ...) do { if (!getenv("STARPU_SSILENT")) {fprintf(ofile, fmt, ## __VA_ARGS__); }} while(0)
#define NX 10

void scal_func(void *buffers[], void *cl_arg)
{
        unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
        float *v = (float *)STARPU_VECTOR_GET_PTR(buffers[0]);
	float factor;

	starpu_codelet_unpack_args(cl_arg, &factor, 0);
        unsigned i;
	for(i=0 ; i<n ; i++)
	{
		v[i] *= factor;
	}
}

struct starpu_codelet scal_codelet =
{
	.cpu_funcs_name = {"scal_func"},
	.nbuffers = 1,
	.modes = {STARPU_RW},
	.name = "scal"
};

int main(int argc, char **argv)
{
	int ret = starpu_init(NULL);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_init");

	int nx = (argc >= 2) ? atoi(argv[1]) : NX;
	float *v = calloc(nx, sizeof(v[0]));
	float factor=3.14;

	int i;
	for(i=0 ; i<nx ; i++) v[i] = i+1.0;

	starpu_data_handle_t array_handle;
	starpu_vector_data_register(&array_handle, STARPU_MAIN_RAM, (uintptr_t)v, nx, sizeof(v[0]));

	ret = starpu_task_insert(&scal_codelet,
				 STARPU_RW, array_handle,
				 STARPU_VALUE, &factor, sizeof(factor),
				 0);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_task_insert");

	starpu_data_unregister(array_handle);

	for(i=0 ; i<nx ; i++)
	{
		FPRINTF(stdout, "v[%d] = %f\n", i, v[i]);
	}

	starpu_shutdown();
}
