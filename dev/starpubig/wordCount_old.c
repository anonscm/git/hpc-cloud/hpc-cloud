/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//package org.apache.spark.examples;
//
//import scala.Tuple2;
//
//import org.apache.spark.api.java.JavaPairRDD;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.sql.SparkSession;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.regex.Pattern;
//
//public final class JavaWordCount {
//  private static final Pattern SPACE = Pattern.compile(" ");
//
//  public static void main(String[] args) throws Exception {
//
//    if (args.length < 1) {
//      System.err.println("Usage: JavaWordCount <file>");
//      System.exit(1);
//    }
//
//    SparkSession spark = SparkSession
//      .builder()
//      .appName("JavaWordCount")
//      .getOrCreate();
//
//    JavaRDD<String> lines = spark.read().textFile(args[0]).javaRDD();
//
//    JavaRDD<String> words = lines.flatMap(s -> Arrays.asList(SPACE.split(s)).iterator());
//
//    JavaPairRDD<String, Integer> ones = words.mapToPair(s -> new Tuple2<>(s, 1));
//
//    JavaPairRDD<String, Integer> counts = ones.reduceByKey((i1, i2) -> i1 + i2);
//
//    List<Tuple2<String, Integer>> output = counts.collect();
//    for (Tuple2<?,?> tuple : output) {
//      System.out.println(tuple._1() + ": " + tuple._2());
//    }
//    spark.stop();
//  }
//}

#include <stdio.h>
#include <stdlib.h>
#include <starpu.h>
#include "uthash.h"

struct _pair_hashlist
{
	UT_hash_handle hh;
	char *word;
	int count;
};

void count_words(char **words, int n, char **pairs_word, int *pairs_count, int n2)
{
        unsigned i;
	struct _pair_hashlist *hashlist = NULL;

        for (i = 0; i < n; i++)
	{
		if (words[i])
		{
			struct _pair_hashlist *res=NULL;
			HASH_FIND_STR(hashlist, words[i], res);
			if (res == NULL)
			{
				res = malloc(sizeof(struct _pair_hashlist));
				res->word = words[i];
				res->count = 1;
				HASH_ADD_STR(hashlist, word, res);
			}
			else
			{
				res->count += 1;
			}
			//fprintf(stderr, "word %d '%20s' (out of %d)\n", i, words[i], n);
		}
	}

	struct _pair_hashlist *current=NULL, *tmp=NULL;
	i=0;
	HASH_ITER(hh, hashlist, current, tmp)
	{
		pairs_word[i] = strdup(current->word);
		pairs_count[i] = current->count;
		//fprintf(stderr, "words '%20s' --> occur %2d\n", pairs_word[i], pairs_count[i]);
		i++;
	}
	//fprintf(stderr, "nb of pairs %d and count %d\n", n2, count);
}

void count_func(void *buffers[], void *cl_arg)
{
        unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
        char **words = (char **)STARPU_VECTOR_GET_PTR(buffers[0]);

        unsigned n2 = STARPU_VECTOR_GET_NX(buffers[1]);
	char **pairs_word = (char **)STARPU_VECTOR_GET_PTR(buffers[1]);
	int *pairs_count = (int *)STARPU_VECTOR_GET_PTR(buffers[2]);

	//fprintf(stderr, "nb of words %d\n", n);
	count_words(words, n, pairs_word, pairs_count, n2);
}

struct starpu_codelet countCodelet =
{
	.cpu_funcs = {count_func},
	.cpu_funcs_name = {"count_func"},
	.nbuffers = 3,
	.modes = {STARPU_R, STARPU_RW, STARPU_RW},
	.name = "count"
};

char **read_words(const char *filename, int *count)
{
	char **words = calloc(1000, sizeof(char *));
	*count=0;
	FILE *f = fopen(filename, "r");
	char line[1024];
	char *delims = " 	";
	while (fgets(line, 1024, f) != NULL && *count != 1000)
	{
		line[strlen(line)-1] = '\0';
		//fprintf(stderr, "reading '%s'\n", line);
		char *word = strtok(line, delims);
		while (word)
		{
			words[*count] = strdup(word);
			*count = *count + 1;
			//fprintf(stderr, "word '%s'\n", word);
			word = strtok(NULL, delims);
		}
	}
	fclose(f);
	return words;
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		fprintf(stderr, "Usage: wordCount <file>\n");
		exit(1);
	}

	int ret = starpu_init(NULL);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_init");

	int count;
	char **words = read_words(argv[1], &count);
	starpu_data_handle_t words_handle;
	starpu_vector_data_register(&words_handle, STARPU_MAIN_RAM, (uintptr_t)words, count, sizeof(char *));

	char **pairs_word = calloc(count, sizeof(char *));
	int *pairs_count = calloc(count, sizeof(int));
	starpu_data_handle_t pairs_word_handle;
	starpu_data_handle_t pairs_count_handle;
	starpu_vector_data_register(&pairs_word_handle, STARPU_MAIN_RAM, (uintptr_t)pairs_word, count, sizeof(pairs_word[0]));
	starpu_vector_data_register(&pairs_count_handle, STARPU_MAIN_RAM, (uintptr_t)pairs_count, count, sizeof(pairs_count[0]));

	struct starpu_data_filter f1 =
	{
	 	.filter_func = starpu_vector_filter_block,
		.nchildren = starpu_cpu_worker_get_count()
	};
	starpu_data_partition(words_handle, &f1);
	starpu_data_partition(pairs_word_handle, &f1);
	starpu_data_partition(pairs_count_handle, &f1);

	int i;
	for(i=0 ; i<starpu_data_get_nb_children(words_handle); i++)
	{
		starpu_data_handle_t words_sub_handle = starpu_data_get_sub_data(words_handle, 1, i);
		starpu_data_handle_t pairs_word_sub_handle = starpu_data_get_sub_data(pairs_word_handle, 1, i);
		starpu_data_handle_t pairs_count_sub_handle = starpu_data_get_sub_data(pairs_count_handle, 1, i);
		ret = starpu_task_insert(&countCodelet,
					 STARPU_R, words_sub_handle,
					 STARPU_RW, pairs_word_sub_handle,
					 STARPU_RW, pairs_count_sub_handle,
					 0);
	}

	starpu_data_unpartition(words_handle, STARPU_MAIN_RAM);
	starpu_data_unregister(words_handle);
	starpu_data_unpartition(pairs_word_handle, STARPU_MAIN_RAM);
	starpu_data_unregister(pairs_word_handle);
	starpu_data_unpartition(pairs_count_handle, STARPU_MAIN_RAM);
	starpu_data_unregister(pairs_count_handle);

	char **pairs_word_final = calloc(count, sizeof(char *));
	int *pairs_count_final = calloc(count, sizeof(int));
	count_words(pairs_word, count, pairs_word_final, pairs_count_final, count);

	i=0;
	while (pairs_count_final[i] != 0)
	{
		fprintf(stderr, "'%s' : %d\n", pairs_word_final[i], pairs_count_final[i]);
		i++;
	}

	starpu_shutdown();
}
