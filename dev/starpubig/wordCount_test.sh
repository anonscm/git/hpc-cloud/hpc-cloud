#!/bin/bash

make wordCount
FILE=${1:-text}
./wordCount $FILE 2>&1 | sort > /tmp/res_starpu_$$.out
./wordCount.sh $FILE 2>&1 | sort > /tmp/res_bash_$$.out
diff /tmp/res_starpu_$$.out /tmp/res_bash_$$.out
res=$?
if test $res -eq 0
then
    cat /tmp/res_starpu_$$.out
    echo
    echo "Success"
else
    echo
    echo "Failure"
fi


