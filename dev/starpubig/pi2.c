/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//package org.apache.spark.examples;
//
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.sql.SparkSession;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Computes an approximation to pi
// * Usage: JavaSparkPi [partitions]
// */
//public final class JavaSparkPi {
//
//  public static void main(String[] args) throws Exception {
//    SparkSession spark = SparkSession
//      .builder()
//      .appName("JavaSparkPi")
//      .getOrCreate();
//
//    JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
//
//    int slices = (args.length == 1) ? Integer.parseInt(args[0]) : 2;
//    int n = 100000 * slices;
//    List<Integer> l = new ArrayList<>(n);
//    for (int i = 0; i < n; i++) {
//      l.add(i);
//    }
//
//    JavaRDD<Integer> dataSet = jsc.parallelize(l, slices);
//
//    int count = dataSet.map(integer -> {
//      double x = Math.random() * 2 - 1;
//      double y = Math.random() * 2 - 1;
//      return (x * x + y * y <= 1) ? 1 : 0;
//    }).reduce((integer, integer2) -> integer + integer2);
//
//    System.out.println("Pi is roughly " + 4.0 * count / n);
//
//    spark.stop();
//  }
//}

#include <stdio.h>
#include <stdlib.h>
#include <starpu.h>

void map_func(void *buffers[], void *cl_arg)
{
        unsigned i;

        unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
        int *val = (int *)STARPU_VECTOR_GET_PTR(buffers[0]);
	unsigned long *count = (unsigned long *)STARPU_VECTOR_GET_PTR(buffers[1]);

        for (i = 0; i < n; i++)
	{
		double x = starpu_drand48() * 2 - 1;
		double y = starpu_drand48() * 2 - 1;
                val[i] = (x * x + y * y <= 1) ? 1 : 0;
		count[0] += val[i];
	}
}

// we could use STARPU_SCRATCH for the 1st data as it is not needed but we want to keep close to the spark java example
struct starpu_codelet mapAndReduceCodelet =
{
	.cpu_funcs = {map_func},
	.cpu_funcs_name = {"map_func"},
	.nbuffers = 2,
	.modes = {STARPU_W, STARPU_W},
	.name = "map"
};

#define NB_SLICES 100000

int main(int argc, char **argv)
{
	int ret = starpu_init(NULL);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_init");

	int slices = (argc >= 2) ? atoi(argv[1]) : 2;
	int n = NB_SLICES * slices;
	unsigned long *count = calloc(NB_SLICES, sizeof(count[0]));
	int i;

	starpu_data_handle_t array_handle;
	starpu_vector_data_register(&array_handle, -1, (uintptr_t)NULL, n, sizeof(int));
	starpu_data_handle_t count_handle;
	starpu_vector_data_register(&count_handle, STARPU_MAIN_RAM, (uintptr_t)count, NB_SLICES, sizeof(count[0]));

	struct starpu_data_filter f =
	{
	 	.filter_func = starpu_vector_filter_block,
		.nchildren = NB_SLICES
	};
	starpu_data_partition(array_handle, &f);
	starpu_data_partition(count_handle, &f);

	for(i=0 ; i<starpu_data_get_nb_children(array_handle); i++)
	{
		starpu_data_handle_t sub_handle = starpu_data_get_sub_data(array_handle, 1, i);
		starpu_data_handle_t sub_count_handle = starpu_data_get_sub_data(count_handle, 1, i);
		ret = starpu_task_insert(&mapAndReduceCodelet,
					 STARPU_W, sub_handle,
					 STARPU_W, sub_count_handle,
					 0);
		STARPU_CHECK_RETURN_VALUE(ret, "starpu_task_insert");
	}

	starpu_data_unpartition(array_handle, STARPU_MAIN_RAM);
	starpu_data_unpartition(count_handle, STARPU_MAIN_RAM);
	starpu_data_unregister(array_handle);
	starpu_data_unregister(count_handle);

	unsigned long all_count=0;
	for(i=0 ; i<NB_SLICES ; i++) all_count += count[i];

	fprintf(stderr, "Pi is roughly %f\n", 4.0 * all_count / n);
	starpu_shutdown();
}
