#!/bin/bash

cat $1 | tr ' ' '\012'| sort | awk 'NR==1 {word=$0 ; count=1} NR != 1 {if ($0 == word) {count += 1} else {printf "%s %d\n", word, count;word=$0;count=1}} END{printf "%s %d\n", word, count}'
