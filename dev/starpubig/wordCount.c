/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//package org.apache.spark.examples;
//
//import scala.Tuple2;
//
//import org.apache.spark.api.java.JavaPairRDD;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.sql.SparkSession;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.regex.Pattern;
//
//public final class JavaWordCount {
//  private static final Pattern SPACE = Pattern.compile(" ");
//
//  public static void main(String[] args) throws Exception {
//
//    if (args.length < 1) {
//      System.err.println("Usage: JavaWordCount <file>");
//      System.exit(1);
//    }
//
//    SparkSession spark = SparkSession
//      .builder()
//      .appName("JavaWordCount")
//      .getOrCreate();
//
//    JavaRDD<String> lines = spark.read().textFile(args[0]).javaRDD();
//
//    JavaRDD<String> words = lines.flatMap(s -> Arrays.asList(SPACE.split(s)).iterator());
//
//    JavaPairRDD<String, Integer> ones = words.mapToPair(s -> new Tuple2<>(s, 1));
//
//    JavaPairRDD<String, Integer> counts = ones.reduceByKey((i1, i2) -> i1 + i2);
//
//    List<Tuple2<String, Integer>> output = counts.collect();
//    for (Tuple2<?,?> tuple : output) {
//      System.out.println(tuple._1() + ": " + tuple._2());
//    }
//    spark.stop();
//  }
//}

#include <stdio.h>
#include <stdlib.h>
#include <starpu.h>
#include "uthash.h"

struct _pair_hashlist
{
	UT_hash_handle hh;
	char *word;
	int count;
};

#define FPRINTF(ofile, fmt, ...) do { if (!getenv("STARPU_SSILENT")) {fprintf(ofile, fmt, ## __VA_ARGS__); }} while(0)

void count_func(void *buffers[], void *cl_arg)
{
        unsigned n = STARPU_VECTOR_GET_NX(buffers[0]);
        char **words = (char **)STARPU_VECTOR_GET_PTR(buffers[0]);
	int *counts = (int *)STARPU_VECTOR_GET_PTR(buffers[1]);

        unsigned n2 = STARPU_VECTOR_GET_NX(buffers[2]);
	char **pairs_word = (char **)STARPU_VECTOR_GET_PTR(buffers[2]);
	int *pairs_count = (int *)STARPU_VECTOR_GET_PTR(buffers[3]);

        unsigned i;
	struct _pair_hashlist *hashlist = NULL;
	for(i=0 ; i<n ; i++)
	{
		if (words[i])
		{
			struct _pair_hashlist *res=NULL;
			HASH_FIND_STR(hashlist, words[i], res);
			if (res == NULL)
			{
				res = malloc(sizeof(struct _pair_hashlist));
				res->word = words[i];
				res->count = counts[i];
				HASH_ADD_STR(hashlist, word, res);
				//FPRINTF(stderr, "(task %p) word %d '%20s' (%d)(out of %d)\n", starpu_task_get_current(), i, words[i], counts[i], n);
			}
			else
			{
				res->count += counts[i];
				//FPRINTF(stderr, "(task %p) word %d '%20s' (+ %d)(out of %d)\n", starpu_task_get_current(), i, words[i], counts[i], n);
			}
		}
	}

	struct _pair_hashlist *current=NULL, *tmp=NULL;
	i=0;
	HASH_ITER(hh, hashlist, current, tmp)
	{
		pairs_word[i] = strdup(current->word);
		pairs_count[i] = current->count;
		//FPRINTF(stderr, "words '%20s' --> occur %2d\n", pairs_word[i], pairs_count[i]);
		i++;
	}
	while (i != n2)
	{
		pairs_word[i] = NULL;
		pairs_count[i] = 0;
		i++;
	}
	//FPRINTF(stderr, "nb of pairs %d and count %d\n", n2, count);
}

struct starpu_codelet count_codelet =
{
	.cpu_funcs = {count_func},
	.cpu_funcs_name = {"count_func"},
	.nbuffers = 4,
	.modes = {STARPU_R, STARPU_R, STARPU_W, STARPU_W},
	.name = "count"
};

void mapToPair_func(void *buffers[], void *cl_arg)
{
	unsigned nb_words = STARPU_VECTOR_GET_NX(buffers[0]);
        char **words = (char **)STARPU_VECTOR_GET_PTR(buffers[0]);
        int *counts = (int *)STARPU_VECTOR_GET_PTR(buffers[1]);

	unsigned i=0;
	while (i != nb_words && words[i])
	{
		counts[i] = 1;
		//FPRINTF(stderr, "word '%20s', count %d (%s)\n", words[i], counts[i], starpu_task_get_name(starpu_task_get_current()));
		i++;
	}
}

struct starpu_codelet mapToPair_codelet =
{
	.cpu_funcs = {mapToPair_func},
	.cpu_funcs_name = {"mapToPair_func"},
	.nbuffers = 2,
	.modes = {STARPU_R, STARPU_W},
	.name = "mapToPair"
};

void flatMap_func(void *buffers[], void *cl_arg)
{
	unsigned cur_line=0;
        unsigned nb_lines = STARPU_VECTOR_GET_NX(buffers[0]);
        char **lines = (char **)STARPU_VECTOR_GET_PTR(buffers[0]);

	unsigned cur_words=0;
	unsigned nb_words = STARPU_VECTOR_GET_NX(buffers[1]);
        char **words = (char **)STARPU_VECTOR_GET_PTR(buffers[1]);

	//	FPRINTF(stderr, "splitting %d lines into at most %d words\n", nb_lines, nb_words);

	char *delims = " 	";
	while (cur_line != nb_lines && cur_words != nb_words)
	{
		char *saveptr;
		//FPRINTF(stderr, "processing line '%s'\n", lines[cur_line]);
		char *word = strtok_r(lines[cur_line], delims, &saveptr);
		while (word)
		{
			words[cur_words] = strdup(word);
			cur_words ++;
			//FPRINTF(stderr, "word '%s'\n", word);
			word = strtok_r(NULL, delims, &saveptr);
		}
		cur_line ++;
	}
	while (cur_words != nb_words)
	{
		words[cur_words] = NULL;
		cur_words++;
	}
}

struct starpu_codelet flatMap_codelet =
{
	.cpu_funcs = {flatMap_func},
	.cpu_funcs_name = {"flatMap_func"},
	.nbuffers = 2,
	.modes = {STARPU_R, STARPU_W},
	.name = "flatMap"
};

char **read_lines(const char *filename, int *nb_lines)
{
	int alloc=100;
	int allocExtra = 10;
	char **lines = malloc(alloc*sizeof(char *));
	*nb_lines=0;
	FILE *f = fopen(filename, "r");
	char line[1024];
	while (fgets(line, 1024, f) != NULL)
	{
		line[strlen(line)-1] = '\0';
		//FPRINTF(stderr, "reading '%s'\n", line);
		lines[*nb_lines] = strdup(line);
		*nb_lines = *nb_lines + 1;
		if (*nb_lines == alloc)
		{
			FPRINTF(stderr, "reallocating %d extra lines\n", allocExtra);
			alloc += allocExtra;
			lines = realloc(lines, alloc*sizeof(char *));
			allocExtra *= 2;
		}
	}
	fclose(f);
	return lines;
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		fprintf(stderr, "Usage: wordCount <file>\n");
		exit(1);
	}

	int ret = starpu_init(NULL);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_init");

	int i;
	struct starpu_data_filter f1 =
	{
	 	.filter_func = starpu_vector_filter_block,
		.nchildren = starpu_cpu_worker_get_count()
	};

	// Read the file into a array of lines
	int nb_lines;
	char **lines = read_lines(argv[1], &nb_lines);
	starpu_data_handle_t lines_handle;
	starpu_vector_data_register(&lines_handle, STARPU_MAIN_RAM, (uintptr_t)lines, nb_lines, sizeof(char *));
	if (nb_lines < f1.nchildren) f1.nchildren = nb_lines;
	starpu_data_partition(lines_handle, &f1);

	// Let's assume each line has at most 20 words (yeap it's wrong ...)
	int nb_words = nb_lines*20;

	// Split all the lines in words
	starpu_data_handle_t words_handle;
	starpu_vector_data_register(&words_handle, -1, (uintptr_t)NULL, nb_words, sizeof(char *));
	starpu_data_partition(words_handle, &f1);
	for(i=0 ; i<starpu_data_get_nb_children(lines_handle) ; i++)
	{
		starpu_data_handle_t lines_sub_handle = starpu_data_get_sub_data(lines_handle, 1, i);
		starpu_data_handle_t words_sub_handle = starpu_data_get_sub_data(words_handle, 1, i);
		ret = starpu_task_insert(&flatMap_codelet,
					 STARPU_R, lines_sub_handle,
					 STARPU_W, words_sub_handle,
					 0);
		STARPU_CHECK_RETURN_VALUE(ret, "starpu_task_insert");
	}
	starpu_data_unpartition(lines_handle, STARPU_MAIN_RAM);
	starpu_data_unregister(lines_handle);

	// Create a tuple per word
	starpu_data_handle_t counts_handle;
	starpu_vector_data_register(&counts_handle, -1, (uintptr_t)NULL, nb_words, sizeof(int));
	starpu_data_partition(counts_handle, &f1);
	for(i=0 ; i<starpu_data_get_nb_children(words_handle) ; i++)
	{
		starpu_data_handle_t words_sub_handle = starpu_data_get_sub_data(words_handle, 1, i);
		starpu_data_handle_t counts_sub_handle = starpu_data_get_sub_data(counts_handle, 1, i);
		ret = starpu_task_insert(&mapToPair_codelet,
					 STARPU_R, words_sub_handle,
					 STARPU_W, counts_sub_handle,
					 0);
		STARPU_CHECK_RETURN_VALUE(ret, "starpu_task_insert");
	}

	// Count the number of words in each partition
	starpu_data_handle_t pairs_word_handle;
	starpu_data_handle_t pairs_count_handle;
	starpu_vector_data_register(&pairs_word_handle, -1, (uintptr_t)NULL, nb_words, sizeof(char *));
	starpu_vector_data_register(&pairs_count_handle, -1, (uintptr_t)NULL, nb_words, sizeof(int));
	starpu_data_partition(pairs_word_handle, &f1);
	starpu_data_partition(pairs_count_handle, &f1);
	for(i=0 ; i<starpu_data_get_nb_children(words_handle) ; i++)
	{
		starpu_data_handle_t words_sub_handle = starpu_data_get_sub_data(words_handle, 1, i);
		starpu_data_handle_t counts_sub_handle = starpu_data_get_sub_data(counts_handle, 1, i);
		starpu_data_handle_t pairs_word_sub_handle = starpu_data_get_sub_data(pairs_word_handle, 1, i);
		starpu_data_handle_t pairs_count_sub_handle = starpu_data_get_sub_data(pairs_count_handle, 1, i);
		ret = starpu_task_insert(&count_codelet,
					 STARPU_R, words_sub_handle,
					 STARPU_R, counts_sub_handle,
					 STARPU_W, pairs_word_sub_handle,
					 STARPU_W, pairs_count_sub_handle,
					 0);
		STARPU_CHECK_RETURN_VALUE(ret, "starpu_task_insert");
	}
	starpu_data_unpartition(words_handle, STARPU_MAIN_RAM);
	starpu_data_unregister(words_handle);
	starpu_data_unpartition(counts_handle, STARPU_MAIN_RAM);
	starpu_data_unregister(counts_handle);

	starpu_data_unpartition(pairs_word_handle, STARPU_MAIN_RAM);
	starpu_data_unpartition(pairs_count_handle, STARPU_MAIN_RAM);

	// Do the reduction
	char **pairs_word_final = calloc(nb_words, sizeof(char *));
	int *pairs_count_final = calloc(nb_words, sizeof(int));
	starpu_data_handle_t pairs_word_final_handle;
	starpu_data_handle_t pairs_count_final_handle;
	starpu_vector_data_register(&pairs_word_final_handle, STARPU_MAIN_RAM, (uintptr_t)pairs_word_final, nb_words, sizeof(char *));
	starpu_vector_data_register(&pairs_count_final_handle, STARPU_MAIN_RAM, (uintptr_t)pairs_count_final, nb_words, sizeof(int));
	ret = starpu_task_insert(&count_codelet,
				 STARPU_R, pairs_word_handle,
				 STARPU_R, pairs_count_handle,
				 STARPU_W, pairs_word_final_handle,
				 STARPU_W, pairs_count_final_handle,
				 0);
	starpu_data_unregister(pairs_word_final_handle);
	starpu_data_unregister(pairs_count_final_handle);

	i=0;
	while (i != nb_words)
	{
		if  (pairs_count_final[i] != 0)
		{
			fprintf(stderr, "%s %d\n", pairs_word_final[i], pairs_count_final[i]);
		}
		i++;
	}

	starpu_data_unregister(pairs_word_handle);
	starpu_data_unregister(pairs_count_handle);

	starpu_shutdown();
}
