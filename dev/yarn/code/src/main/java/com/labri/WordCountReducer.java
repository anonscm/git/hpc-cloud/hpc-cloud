package com.labri;

import java.io.IOException;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

// le premier <Text, IntWritable> est la sortie du map (IntWritable au lieu de Int pour être serialisable)
// le deuxieme <Text, IntWritable> est la sortie du reduce
public class WordCountReducer
	extends Reducer<Text,IntWritable,Text,IntWritable>
{
	// reduce est appelee pour chaque clef différente resultante du map
	public void reduce(Text key, Iterable<IntWritable> values,
			   Context context) throws IOException, InterruptedException
	{
		int sum = 0;
		for (IntWritable val : values)
		{
			// val vaut toujours 1 si on n'a pas utilisé le combiner
			System.out.println("key = <" + key + "> value = <" + val + ">");
			sum += val.get();
		}
		IntWritable result = new IntWritable(sum);
		context.write(key, result);
	}

}
