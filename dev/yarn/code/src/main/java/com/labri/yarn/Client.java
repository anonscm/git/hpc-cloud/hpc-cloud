package com.labri.yarn;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.CreateFlag;
import org.apache.hadoop.fs.FileContext;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.conf.YarnConfiguration;

import java.io.*;
import java.net.Socket;
import java.util.EnumSet;
import java.util.Map;
import java.util.Random;

public class Client {
    private static final Log LOG = LogFactory.getLog(Client.class);

    int clientId;
    String serverAddr;
    int serverPort;

    Client(int id, String address, int port) {
        clientId = id;
        serverAddr = address;
        serverPort = port;
    }

    void run(PrintWriter out) {
        try {
            int t = new Random().nextInt(5000);
            LOG.warn ("sleeping for "+t);

            Thread.sleep(t);
            Socket socket = new Socket(serverAddr, serverPort);

            DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
            writer.writeInt(clientId);
            writer.flush();

            DataInputStream reader = new DataInputStream(socket.getInputStream());
            int c = reader.readInt();
            int ack = Server.ack(clientId);
            if (c == ack)
                out.println(String.format("client %d: receive ack from server.", clientId));
            else
                out.println(String.format("invalid ack %d != %d.", ack, c));
            writer.close();
            reader.close();
            socket.close();
        } catch (Exception e) {
            LOG.error(String.format("client %d: %s", clientId,e.getMessage()));
        }
    }

    public void run() {
        LOG.warn(new YarnConfiguration());
        Map<String, String> envs = System.getenv();

        LOG.warn("---- ENVIRONMENT");
        for(String k : envs.keySet()) {
            LOG.warn(k);
        }
        LOG.warn("------------------");

        try {

            FileContext fc = FileContext.getFileContext();
            Path path = new Path(fc.getHomeDirectory() +"/client"+ clientId);
            PrintWriter out = new PrintWriter(fc.create(path, EnumSet.of(CreateFlag.CREATE)));
            LOG.warn("out to " + path + " is "+(out==null?"":"not")+" null");
            run(out);
            out.flush();
            out.close();
        } catch (IOException e) {
            LOG.error(String.format("client %d: %s", clientId, e.getMessage()));
        }
    }

    public static void main(String[] args) throws Exception {

        if (args.length != 3) {
            LOG.error("missing arguments : client-id server-addr server-port");
            System.exit(1);
        }
        Random rnd = new Random();
        int delay = rnd.nextInt(5000);
        Thread.sleep(delay);
        int id = Integer.parseInt(args[0]);
        String addr = args[1];
        int port = Integer.parseInt(args[2]);
        new Client(id, addr, port).run();
    }
}
