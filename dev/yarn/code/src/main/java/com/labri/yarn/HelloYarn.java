/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.labri.yarn;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.Random;

public class HelloYarn extends Client {
  private static final long MEGABYTE = 1024L * 1024L;

	//  private static final Log LOG = LogFactory.getLog(HelloYarn.class);

  public HelloYarn(int id, String serverHost, int serverPort) {
    super(id, serverHost, serverPort);
    System.out.println("HelloYarn!");
  }

  public static long bytesToMegabytes(long bytes) {
    return bytes / MEGABYTE;
  }

  public void printMemoryStats() {
    long freeMemory = bytesToMegabytes(Runtime.getRuntime().freeMemory());
    long totalMemory = bytesToMegabytes(Runtime.getRuntime().totalMemory());
    long maxMemory = bytesToMegabytes(Runtime.getRuntime().maxMemory());

    Map<String, String> envs = System.getenv();
    if (envs.containsKey("MASTER_HOSTNAME")) {
      System.out.println(String.format("MASTER_HOSTNAME = %s", envs.get("MASTER_HOSTNAME")));
    } else
      System.err.println("No MASTER_HOSTNAME in environment.");

    System.out.println("The amount of free memory in the Java Virtual Machine: " + freeMemory);
    System.out.println("The total amount of memory in the Java virtual machine: " + totalMemory);
    System.out.println("The maximum amount of memory that the Java virtual machine: " + maxMemory);
    run();
    System.out.println("Client Terminated");
  }

  public static void main(String[] args) {
	  //      LOG.info("Starting Hello");
	  System.err.println("Hello, this is a test");
	  int id = new Random().nextInt();
	  String serverName = System.getenv("MASTER_HOSTNAME");
      int serverPort = Integer.parseInt(System.getenv("MASTER_PORT"));
	  HelloYarn helloYarn = new HelloYarn(id, serverName, serverPort);
	  helloYarn.printMemoryStats();
	  System.exit(1);
  }
}
