package com.labri.yarn;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.CreateFlag;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileContext;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.apache.hadoop.yarn.conf.YarnConfiguration;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.EnumSet;

public class Server implements Runnable {
    private static final Log LOG = LogFactory.getLog(Server.class);

    int port;
    int nbClients;
    ServerSocket socket;

    public Server(int nbClients, int port) throws IOException {
        this.port = port;
        this.nbClients = nbClients;
        socket = new ServerSocket(port);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        socket.close();
    }

    public static int ack(int clientId) {
        return (3*clientId^19);
    }

    public void run(PrintWriter out) {
        try {
            LOG.warn(String.format("starting server for %d clients", nbClients));
            Thread[] tasks = new Thread[nbClients];

            for (int i = 0; i < nbClients; i++) {
                try {
                    final Socket client = socket.accept();

                    tasks[i] = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                out.println("New connection from " + client.getInetAddress());
                                DataInputStream reader = new DataInputStream(client.getInputStream());
                                int clientIndex = reader.readInt();
                                out.println("client : " + clientIndex + " from " + client.getInetAddress());

                                DataOutputStream writer = new DataOutputStream(client.getOutputStream());
                                writer.writeInt(ack(clientIndex));
                                writer.flush();

                                reader.close();
                                writer.close();
                                client.close();
                            } catch (IOException e) {
                                out.println("error:" + e.getMessage());
                            }
                        }
                    });
                    tasks[i].start();

                } catch (IOException e) {
                    out.println("error:" + e.getMessage());
                }
            }
            for (int i = 0; i < nbClients; i++) {
                try {
                    tasks[i].join();
                } catch (InterruptedException e) {
                    out.println("error:" + e.getMessage());
                }
            }
        } catch (Exception e) {
            LOG.error (e.getMessage());
        }
        LOG.warn(String.format("terminate server run()"));
    }

    @Override
    public void run() {
        try {
            LOG.warn(new Configuration());
            FileContext fc = FileContext.getFileContext();
            Path path = new Path(fc.getHomeDirectory()+ "/server");
            PrintWriter out = new PrintWriter(fc.create(path, EnumSet.of(CreateFlag.CREATE)));
            if (out == null) {
                LOG.error("out is null");
            } else {
                LOG.warn("out to "+ path+ " is not null");
            }
            run(out);
            out.flush();
            out.close();
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
        LOG.warn(String.format("terminate server run()"));
    }

    public static Server startServer(int nbClients, int port) throws IOException {
        Server server = new Server(nbClients, port);
        new Thread(server).start();
        return server;
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            LOG.error("missing arguments: nb-clients port");
            System.exit(1);
        }
        try {
            Server.startServer(Integer.parseInt(args[0]),Integer.parseInt(args[1]));
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }
}
