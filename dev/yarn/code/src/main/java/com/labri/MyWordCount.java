package com.labri;

/**
 * Hello world!
 *
 */
import java.io.IOException;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;

public class MyWordCount
{
	public static void main(String[] args) throws Exception
	{
		// conf permet d'affiner l'execution du job et
		// contient a la creation toute la conf par defaut
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if (otherArgs.length != 2)
		{
			System.err.println("Usage: wordcount <in> <out>");
			System.exit(2);
		}

		// demande a modifier le nombre de cores qui va
		// executer le map
		conf.set("mapreduce.map.cpu.vcores", "4");
		// 2eme possibilite
		conf.set(Job.MAP_CPU_VCORES, "4");

		// cree le job
		Job job = Job.getInstance(conf);

		// indique a hadoop dans quel jar vont se trouver le
		// mapper, reducer et combiner, utile quand l'appli
		// est exécutée via yarn
		job.setJarByClass(MyWordCount.class);

		// classe qui correspond a la fonction Map, si on ne
		// met rien, la methode par defaut ne fait que
		// renvoyer l'entree telle quelle
		job.setMapperClass(WordCountMapper.class);

		// classe qui correspond a la fonction Reduce, si on
		// ne met rien, la methode par defaut ne fait que
		// renvoyer l'entrée telle quelle
		job.setReducerClass(WordCountReducer.class);

		// le combiner permet de faire une reduction locale au
		// niveau du mapper
		job.setCombinerClass(WordCountReducer.class);

		// par defaut,les types attendus sont Text
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		// defini la norme des donnees recus en entree
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

		//FileOutputFormat.setCompressOutput(job, true);

		// soumet le job et attend sa terminaison
		int res = job.waitForCompletion(true) ? 0 : 1;
		System.exit(res);
	}

}
