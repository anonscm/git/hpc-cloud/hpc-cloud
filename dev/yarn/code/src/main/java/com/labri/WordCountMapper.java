package com.labri;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

// LongWritable, Text correspond au premier ensemble de tuple <key, value> fourni par hadoop
// on peut utiliser Object au lien de LongWritable si on se fiche de la valeur de la clef
// Text, IntWritable est la sortie du map (IntWritable au lieu de Int pour être serialisable
public class WordCountMapper
	extends Mapper<LongWritable, Text, Text, IntWritable>
{
	// map est appelé pour chaque ligne de texte du fichier d'entree
	// key est le numero de la ligne, value est le contenu de la ligne
	public void map(LongWritable key, Text value,
			Context context) throws IOException, InterruptedException
	{
		StringTokenizer itr = new StringTokenizer(value.toString());
		while (itr.hasMoreTokens())
		{
			Text newkey = new Text(itr.nextToken());
			IntWritable newvalue = new IntWritable(1);
			context.write(newkey, newvalue);
		}
	}

}
