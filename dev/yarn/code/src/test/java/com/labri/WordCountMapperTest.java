package com.labri;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.mockito.Mock;
import org.mockito.InOrder;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WordCountMapperTest
{
	// ici on pourrait écrire WordCountMapper.Context mais au cas
	// où cette classe redéfinit Context, ce n'est pas celle la
	// qu'on veut utiliser dans le test, car dans une vraie
	// exécution, hadoop crée un context de la classe Mapper et
	// non celle de WordCountMapper
	// l'annotation @Mock est utilisée par junit qui intercepte
	// l'exécution de la classe, qui va instancier le type en
	// fonction du ExtendWith défini au niveau de la classe
	@Mock
	private Mapper<LongWritable, Text, Text, IntWritable>.Context mockContext;

	@Test
	void test() throws Exception
	{
		WordCountMapper mapper = new WordCountMapper();
		String input = "Bonjour le monde";

		mapper.map(new LongWritable(0), new Text(input), mockContext);
		//fail("not yet implemented");

		// vérifie que la méthode write a été appelée 3 fois
		// avec un objet Text et un objet IntWritable
		verify(mockContext, times(3)).write(any(Text.class), any(IntWritable.class));

		verify(mockContext, atLeastOnce()).write(eq(new Text("Bonjour")), eq(new IntWritable(1)));
		verify(mockContext, times(1)).write(eq(new Text("le")), eq(new IntWritable(1)));
		verify(mockContext, times(1)).write(eq(new Text("monde")), eq(new IntWritable(1)));

		InOrder verifyInOrder = inOrder(mockContext);
		verifyInOrder.verify(mockContext).write(eq(new Text("Bonjour")), eq(new IntWritable(1)));
		verifyInOrder.verify(mockContext).write(eq(new Text("le")), eq(new IntWritable(1)));
		verifyInOrder.verify(mockContext).write(eq(new Text("monde")), eq(new IntWritable(1)));
	}
}
